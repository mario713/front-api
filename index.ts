import type * as types from './types';
import type { ConfigOptions, FetchResponse } from 'api/dist/core'
import Oas from 'oas';
import APICore from 'api/dist/core';
import definition from './openapi.json';

class SDK {
  spec: Oas;
  core: APICore;

  constructor() {
    this.spec = Oas.init(definition);
    this.core = new APICore(this.spec, 'front/1.0.0 (api/6.1.1)');
  }

  /**
   * Optionally configure various options that the SDK allows.
   *
   * @param config Object of supported SDK options and toggles.
   * @param config.timeout Override the default `fetch` request timeout of 30 seconds. This number
   * should be represented in milliseconds.
   */
  config(config: ConfigOptions) {
    this.core.setConfig(config);
  }

  /**
   * If the API you're using requires authentication you can supply the required credentials
   * through this method and the library will magically determine how they should be used
   * within your API request.
   *
   * With the exception of OpenID and MutualTLS, it supports all forms of authentication
   * supported by the OpenAPI specification.
   *
   * @example <caption>HTTP Basic auth</caption>
   * sdk.auth('username', 'password');
   *
   * @example <caption>Bearer tokens (HTTP or OAuth 2)</caption>
   * sdk.auth('myBearerToken');
   *
   * @example <caption>API Keys</caption>
   * sdk.auth('myApiKey');
   *
   * @see {@link https://spec.openapis.org/oas/v3.0.3#fixed-fields-22}
   * @see {@link https://spec.openapis.org/oas/v3.1.0#fixed-fields-22}
   * @param values Your auth credentials for the API; can specify up to two strings or numbers.
   */
  auth(...values: string[] | number[]) {
    this.core.setAuth(...values);
    return this;
  }

  /**
   * If the API you're using offers alternate server URLs, and server variables, you can tell
   * the SDK which one to use with this method. To use it you can supply either one of the
   * server URLs that are contained within the OpenAPI definition (along with any server
   * variables), or you can pass it a fully qualified URL to use (that may or may not exist
   * within the OpenAPI definition).
   *
   * @example <caption>Server URL with server variables</caption>
   * sdk.server('https://{region}.api.example.com/{basePath}', {
   *   name: 'eu',
   *   basePath: 'v14',
   * });
   *
   * @example <caption>Fully qualified server URL</caption>
   * sdk.server('https://eu.api.example.com/v14');
   *
   * @param url Server URL
   * @param variables An object of variables to replace into the server URL.
   */
  server(url: string, variables = {}) {
    this.core.setServer(url, variables);
  }

  /**
   * List the accounts of the company.
   *
   * @summary List Accounts
   */
  listAccounts(metadata?: types.ListAccountsMetadataParam): Promise<FetchResponse<200, types.ListAccountsResponse200>> {
    return this.core.fetch('/accounts', 'get', metadata);
  }

  /**
   * Create a new account.
   *
   * @summary Create account
   */
  createAccount(body: types.CreateAccountBodyParam): Promise<FetchResponse<201, types.CreateAccountResponse201>> {
    return this.core.fetch('/accounts', 'post', body);
  }

  /**
   * Lists the custom fields that can be attached to an Account.
   *
   * @summary List Account's custom fields
   */
  listAccountCustomFields(): Promise<FetchResponse<200, types.ListAccountCustomFieldsResponse200>> {
    return this.core.fetch('/accounts/custom_fields', 'get');
  }

  /**
   * Fetches an account
   *
   * @summary Fetch an account
   */
  fetchAnAccount(metadata: types.FetchAnAccountMetadataParam): Promise<FetchResponse<200, types.FetchAnAccountResponse200>> {
    return this.core.fetch('/accounts/{account_id}', 'get', metadata);
  }

  /**
   * Updates an account.
   *
   * @summary Update account
   */
  updateAccount(body: types.UpdateAccountBodyParam, metadata: types.UpdateAccountMetadataParam): Promise<FetchResponse<200, types.UpdateAccountResponse200>>;
  updateAccount(metadata: types.UpdateAccountMetadataParam): Promise<FetchResponse<200, types.UpdateAccountResponse200>>;
  updateAccount(body?: types.UpdateAccountBodyParam | types.UpdateAccountMetadataParam, metadata?: types.UpdateAccountMetadataParam): Promise<FetchResponse<200, types.UpdateAccountResponse200>> {
    return this.core.fetch('/accounts/{account_id}', 'patch', body, metadata);
  }

  /**
   * Deletes an account
   *
   * @summary Delete an account
   */
  deleteAnAccount(metadata: types.DeleteAnAccountMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/accounts/{account_id}', 'delete', metadata);
  }

  /**
   * Lists the contacts associated with an Account
   *
   * @summary List account contacts
   */
  listAccountContacts(metadata: types.ListAccountContactsMetadataParam): Promise<FetchResponse<200, types.ListAccountContactsResponse200>> {
    return this.core.fetch('/accounts/{account_id}/contacts', 'get', metadata);
  }

  /**
   * Adds a list of contacts to an Account
   *
   * @summary Add contact to Account
   */
  addContactToAccount(body: types.AddContactToAccountBodyParam, metadata: types.AddContactToAccountMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/accounts/{account_id}/contacts', 'post', body, metadata);
  }

  /**
   * Removes a list of contacts from an Account
   *
   * @summary Remove contact from Account
   */
  removeContactFromAccount(body: types.RemoveContactFromAccountBodyParam, metadata: types.RemoveContactFromAccountMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/accounts/{account_id}/contacts', 'delete', body, metadata);
  }

  /**
   * Create a new analytics export of messages or events (activities) over a specific time
   * span.
   * The export will be executed asynchronously. The response will include a link that can be
   * used to retrieve the export status & result. Refer to the
   * [Analytics](https://dev.frontapp.com/reference/analytics) topic for details about
   * specific metrics.
   *
   *
   * @summary Create a new analytics export.
   */
  createAnalyticsExport(body?: types.CreateAnalyticsExportBodyParam): Promise<FetchResponse<201, types.CreateAnalyticsExportResponse201>> {
    return this.core.fetch('/analytics/exports', 'post', body);
  }

  /**
   * Fetch an analytics exports. Refer to the
   * [Analytics](https://dev.frontapp.com/reference/analytics) topic for details about
   * specific metrics.
   *
   * @summary Fetch an analytics export.
   */
  getAnalyticsExport(metadata: types.GetAnalyticsExportMetadataParam): Promise<FetchResponse<200, types.GetAnalyticsExportResponse200>> {
    return this.core.fetch('/analytics/exports/{export_id}', 'get', metadata);
  }

  /**
   * Create a new analytics report for a set of metrics over a specific time span. Different
   * filters (e.g. Inbox v Tag v Teammates) will be joined with AND logic, but the IDs within
   * a filter will be joined with OR logic (e.g. Inbox A or Inbox B, Tag A or Tag B).
   * The report will be executed asynchronously. The response will include a link that can be
   * used to retrieve the
   * report status & result. Refer to the
   * [Analytics](https://dev.frontapp.com/reference/analytics) topic for details about
   * specific metrics.
   *
   *
   * @summary Create a new analytics report.
   */
  createAnalyticsReport(body: types.CreateAnalyticsReportBodyParam): Promise<FetchResponse<201, types.CreateAnalyticsReportResponse201>> {
    return this.core.fetch('/analytics/reports', 'post', body);
  }

  /**
   * Fetch an analytics report. Refer to the
   * [Analytics](https://dev.frontapp.com/reference/analytics) topic for details about
   * specific metrics.
   *
   * @summary Fetch an analytics report.
   */
  getAnalyticsReport(metadata: types.GetAnalyticsReportMetadataParam): Promise<FetchResponse<200, types.GetAnalyticsReportResponse200>> {
    return this.core.fetch('/analytics/reports/{report_uid}', 'get', metadata);
  }

  /**
   * List the channels of the company.
   *
   * @summary List channels
   */
  listChannels(): Promise<FetchResponse<200, types.ListChannelsResponse200>> {
    return this.core.fetch('/channels', 'get');
  }

  /**
   * Fetch a channel.
   *
   * @summary Get channel
   */
  getChannel(metadata: types.GetChannelMetadataParam): Promise<FetchResponse<200, types.GetChannelResponse200>> {
    return this.core.fetch('/channels/{channel_id}', 'get', metadata);
  }

  /**
   * Update a channel.
   *
   * @summary Update Channel
   */
  updateChannel(body: types.UpdateChannelBodyParam, metadata: types.UpdateChannelMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateChannel(metadata: types.UpdateChannelMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateChannel(body?: types.UpdateChannelBodyParam | types.UpdateChannelMetadataParam, metadata?: types.UpdateChannelMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/channels/{channel_id}', 'patch', body, metadata);
  }

  /**
   * Create a draft message which is the first message of a new
   * [conversation](https://dev.frontapp.com/reference/conversations).
   *
   * @summary Create draft
   */
  createDraft(body: types.CreateDraftBodyParam, metadata: types.CreateDraftMetadataParam): Promise<FetchResponse<200, types.CreateDraftResponse200>> {
    return this.core.fetch('/channels/{channel_id}/drafts', 'post', body, metadata);
  }

  /**
   * Receive a custom message in Front. This endpoint is available for custom channels
   * **ONLY**.
   *
   * @summary Receive custom messages
   */
  receiveCustomMessages(body: types.ReceiveCustomMessagesBodyParam, metadata: types.ReceiveCustomMessagesMetadataParam): Promise<FetchResponse<202, types.ReceiveCustomMessagesResponse202>> {
    return this.core.fetch('/channels/{channel_id}/incoming_messages', 'post', body, metadata);
  }

  /**
   * Send a new message from a channel. This is one of the ways to create a new
   * [conversation](https://dev.frontapp.com/reference/conversations#creating-a-new-conversation).
   * The new conversation will support both messages and comments (discussions).
   *
   * @summary Create message
   */
  createMessage(body: types.CreateMessageBodyParam, metadata: types.CreateMessageMetadataParam): Promise<FetchResponse<202, types.CreateMessageResponse202>> {
    return this.core.fetch('/channels/{channel_id}/messages', 'post', body, metadata);
  }

  /**
   * Asynchronously validate an SMTP channel (this endpoint is irrelevant to other channel
   * types). When you create an SMTP channel via the API, [create a
   * channel](https://dev.frontapp.com/reference/post_inboxes-inbox-id-channels) with type
   * smtp and the send_as set to the needed email address. You then [configure the email
   * provider](https://help.front.com/en/articles/2081), after which you use this endpoint to
   * asynchronously validate the SMTP settings.
   *
   * @summary Validate channel
   */
  validateChannel(metadata: types.ValidateChannelMetadataParam): Promise<FetchResponse<202, types.ValidateChannelResponse202>> {
    return this.core.fetch('/channels/{channel_id}/validate', 'post', metadata);
  }

  /**
   * Fetches a comment.
   *
   * @summary Get comment
   */
  getComment(metadata: types.GetCommentMetadataParam): Promise<FetchResponse<200, types.GetCommentResponse200>> {
    return this.core.fetch('/comments/{comment_id}', 'get', metadata);
  }

  /**
   * List the teammates mentioned in a comment.
   *
   * @summary List comment mentions
   */
  listCommentMentions(metadata: types.ListCommentMentionsMetadataParam): Promise<FetchResponse<200, types.ListCommentMentionsResponse200>> {
    return this.core.fetch('/comments/{comment_id}/mentions', 'get', metadata);
  }

  /**
   * List the company rules.
   *
   * @summary List all company rules
   */
  listAllCompanyRules(): Promise<FetchResponse<200, types.ListAllCompanyRulesResponse200>> {
    return this.core.fetch('/company/rules', 'get');
  }

  /**
   * List the company tags.
   *
   * @summary List company tags
   */
  listCompanyTags(metadata?: types.ListCompanyTagsMetadataParam): Promise<FetchResponse<200, types.ListCompanyTagsResponse200>> {
    return this.core.fetch('/company/tags', 'get', metadata);
  }

  /**
   * Create a company tag.
   *
   * @summary Create company tag
   */
  createCompanyTag(body: types.CreateCompanyTagBodyParam): Promise<FetchResponse<201, types.CreateCompanyTagResponse201>> {
    return this.core.fetch('/company/tags', 'post', body);
  }

  /**
   * List the contact groups.
   *
   * @summary List groups
   */
  listGroups(): Promise<FetchResponse<200, types.ListGroupsResponse200>> {
    return this.core.fetch('/contact_groups', 'get');
  }

  /**
   * Create a new contact group in the default team (workspace).
   *
   * @summary Create group
   */
  createGroup(body: types.CreateGroupBodyParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/contact_groups', 'post', body);
  }

  /**
   * Delete a contact group.
   *
   * @summary Delete group
   */
  deleteGroup(metadata: types.DeleteGroupMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/contact_groups/{contact_group_id}', 'delete', metadata);
  }

  /**
   * List the contacts belonging to the requested group.
   *
   * @summary List contacts in group
   */
  listContactsInGroup(metadata: types.ListContactsInGroupMetadataParam): Promise<FetchResponse<200, types.ListContactsInGroupResponse200>> {
    return this.core.fetch('/contact_groups/{contact_group_id}/contacts', 'get', metadata);
  }

  /**
   * Add contacts to the requested group.
   *
   * @summary Add contacts to group
   */
  addContactsToGroup(body: types.AddContactsToGroupBodyParam, metadata: types.AddContactsToGroupMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/contact_groups/{contact_group_id}/contacts', 'post', body, metadata);
  }

  /**
   * Remove contacts from the requested group.
   *
   * @summary Remove contacts from group
   */
  removeContactsFromGroup(body: types.RemoveContactsFromGroupBodyParam, metadata: types.RemoveContactsFromGroupMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/contact_groups/{contact_group_id}/contacts', 'delete', body, metadata);
  }

  /**
   * List the contacts of the company.
   *
   * @summary List contacts
   */
  listContacts(metadata?: types.ListContactsMetadataParam): Promise<FetchResponse<200, types.ListContactsResponse200>> {
    return this.core.fetch('/contacts', 'get', metadata);
  }

  /**
   * Create a new contact.
   *
   * @summary Create contact
   */
  createContact(body: types.CreateContactBodyParam): Promise<FetchResponse<201, types.CreateContactResponse201>> {
    return this.core.fetch('/contacts', 'post', body);
  }

  /**
   * Lists the custom fields that can be attached to a Contact.
   *
   * @summary List Contact's custom fields
   */
  listContactCustomFields(): Promise<FetchResponse<200, types.ListContactCustomFieldsResponse200>> {
    return this.core.fetch('/contacts/custom_fields', 'get');
  }

  /**
   * Merges the contacts specified into a single contact, deleting the merged-in contacts.
   * If a target contact ID is supplied, the other contacts will be merged into that one.
   * Otherwise, some contact in the contact ID list will be treated as the target contact.
   * Merge conflicts will be resolved in the following ways:
   *   * name will prioritize manually-updated and non-private contact names
   *   * descriptions will be concatenated and separated by newlines in order from
   *     oldest to newest with the (optional) target contact's description first
   *   * all handles, groups, links, and notes will be preserved
   *   * other conflicts will use the most recently updated contact's value
   *
   *
   * @summary Merge contacts
   */
  mergeContacts(body: types.MergeContactsBodyParam): Promise<FetchResponse<200, types.MergeContactsResponse200>> {
    return this.core.fetch('/contacts/merge', 'post', body);
  }

  /**
   * Fetch a contact.
   *
   * @summary Get contact
   */
  getContact(metadata: types.GetContactMetadataParam): Promise<FetchResponse<200, types.GetContactResponse200>> {
    return this.core.fetch('/contacts/{contact_id}', 'get', metadata);
  }

  /**
   * Updates a contact.
   *
   * @summary Update a contact
   */
  updateAContact(body: types.UpdateAContactBodyParam, metadata: types.UpdateAContactMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateAContact(metadata: types.UpdateAContactMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateAContact(body?: types.UpdateAContactBodyParam | types.UpdateAContactMetadataParam, metadata?: types.UpdateAContactMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/contacts/{contact_id}', 'patch', body, metadata);
  }

  /**
   * Delete a contact.
   *
   * @summary Delete a contact
   */
  deleteAContact(metadata: types.DeleteAContactMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/contacts/{contact_id}', 'delete', metadata);
  }

  /**
   * List the conversations for a contact in reverse chronological order (newest first). For
   * more advanced filtering, see the [search
   * endpoint](https://dev.frontapp.com/reference/conversations#search-conversations).
   * > ⚠️ Deprecated field included
   * >
   * > This endpoint returns a deprecated `last_message` field in the top-level conversation
   * bodies listed. Please use the
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary List contact conversations
   */
  listContactConversations(metadata: types.ListContactConversationsMetadataParam): Promise<FetchResponse<200, types.ListContactConversationsResponse200>> {
    return this.core.fetch('/contacts/{contact_id}/conversations', 'get', metadata);
  }

  /**
   * Adds a new handle to a contact.
   *
   * @summary Add contact handle
   */
  addContactHandle(body: types.AddContactHandleBodyParam, metadata: types.AddContactHandleMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/contacts/{contact_id}/handles', 'post', body, metadata);
  }

  /**
   * Remove a handle from a contact.
   *
   * @summary Delete contact handle
   */
  deleteContactHandle(body: types.DeleteContactHandleBodyParam, metadata: types.DeleteContactHandleMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/contacts/{contact_id}/handles', 'delete', body, metadata);
  }

  /**
   * List the notes added to a contact.
   *
   * @summary List notes
   */
  listNotes(metadata: types.ListNotesMetadataParam): Promise<FetchResponse<202, types.ListNotesResponse202>> {
    return this.core.fetch('/contacts/{contact_id}/notes', 'get', metadata);
  }

  /**
   * Create a new note on a contact.
   *
   * @summary Add note
   */
  addNote(body: types.AddNoteBodyParam, metadata: types.AddNoteMetadataParam): Promise<FetchResponse<201, types.AddNoteResponse201>> {
    return this.core.fetch('/contacts/{contact_id}/notes', 'post', body, metadata);
  }

  /**
   * List the conversations in the company in reverse chronological order (most recently
   * updated first). The order will respect your company's [bump
   * settings](https://help.front.com/t/y729th/customize-when-conversations-bump-up), which
   * determine when conversations bump to the top. For more advanced filtering, see the
   * [search
   * endpoint](https://dev.frontapp.com/reference/conversations#search-conversations).
   * > ⚠️ Deprecated field included
   * >
   * > This endpoint returns a deprecated `last_message` field in the top-level conversation
   * bodies listed. Please use the
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary List conversations
   */
  listConversations(metadata?: types.ListConversationsMetadataParam): Promise<FetchResponse<200, types.ListConversationsResponse200>> {
    return this.core.fetch('/conversations', 'get', metadata);
  }

  /**
   * Create a new
   * [conversation](https://dev.frontapp.com/reference/conversations#creating-a-new-conversation)
   * that only supports comments (known as discussions in Front). If you want to create a
   * conversation that supports messages, use the [Create
   * message](https://dev.frontapp.com/reference/post_channels-channel-id-messages) endpoint.
   * If you want to add a comment to an existing conversation, use the [Add
   * comment](https://dev.frontapp.com/reference/post_conversations-conversation-id-comments)
   * endpoint.
   *
   * @summary Create discussion conversation
   */
  createConversation(body: types.CreateConversationBodyParam): Promise<FetchResponse<201, types.CreateConversationResponse201>> {
    return this.core.fetch('/conversations', 'post', body);
  }

  /**
   * Lists the custom fields that can be attached to a Conversation.
   *
   * @summary List Conversation's custom fields
   */
  listConversationCustomFields(): Promise<FetchResponse<200, types.ListConversationCustomFieldsResponse200>> {
    return this.core.fetch('/conversations/custom_fields', 'get');
  }

  /**
   * Search for conversations. Response will include a count of total matches and an array of
   * conversations in descending order by last activity.
   * See the [search syntax documentation](https://dev.frontapp.com/docs/search-1) for usage
   * examples.
   * **Note:** This endpoint is subject to [proportional rate
   * limiting](https://dev.frontapp.com/docs/rate-limiting#additional-proportional-limiting)
   * at 40% of your company's rate limit.
   * > ⚠️ Deprecated field included
   * >
   * > This endpoint returns a deprecated `last_message` field in the top-level conversation
   * bodies listed. Please use the
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary Search conversations
   */
  searchConversations(metadata: types.SearchConversationsMetadataParam): Promise<FetchResponse<200, types.SearchConversationsResponse200>> {
    return this.core.fetch('/conversations/search/{query}', 'get', metadata);
  }

  /**
   * Fetch a conversation.
   * > ⚠️ Deprecated field included
   * >
   * > This endpoint returns a deprecated `last_message` field in the main body. Please use
   * the
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary Get conversation
   */
  getConversationById(metadata: types.GetConversationByIdMetadataParam): Promise<FetchResponse<200, types.GetConversationByIdResponse200>> {
    return this.core.fetch('/conversations/{conversation_id}', 'get', metadata);
  }

  /**
   * Update a conversation.
   *
   * @summary Update conversation
   */
  updateConversation(body: types.UpdateConversationBodyParam, metadata: types.UpdateConversationMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateConversation(metadata: types.UpdateConversationMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateConversation(body?: types.UpdateConversationBodyParam | types.UpdateConversationMetadataParam, metadata?: types.UpdateConversationMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}', 'patch', body, metadata);
  }

  /**
   * Assign or unassign a conversation.
   *
   * @summary Update conversation assignee
   */
  updateConversationAssignee(body: types.UpdateConversationAssigneeBodyParam, metadata: types.UpdateConversationAssigneeMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}/assignee', 'put', body, metadata);
  }

  /**
   * List the comments in a conversation in reverse chronological order (newest first).
   *
   * @summary List conversation comments
   */
  listConversationComments(metadata: types.ListConversationCommentsMetadataParam): Promise<FetchResponse<200, types.ListConversationCommentsResponse200>> {
    return this.core.fetch('/conversations/{conversation_id}/comments', 'get', metadata);
  }

  /**
   * Add a comment to a [conversation](https://dev.frontapp.com/reference/conversations). If
   * you want to create a new comment-only conversation, use the [Create discussion
   * conversation](https://dev.frontapp.com/reference/create-conversation) endpoint.
   *
   * @summary Add comment
   */
  addComment(body: types.AddCommentBodyParam, metadata: types.AddCommentMetadataParam): Promise<FetchResponse<201, types.AddCommentResponse201>> {
    return this.core.fetch('/conversations/{conversation_id}/comments', 'post', body, metadata);
  }

  /**
   * List the drafts in a conversation.
   *
   * @summary List conversation drafts
   */
  listConversationDrafts(metadata: types.ListConversationDraftsMetadataParam): Promise<FetchResponse<200, types.ListConversationDraftsResponse200>> {
    return this.core.fetch('/conversations/{conversation_id}/drafts', 'get', metadata);
  }

  /**
   * Create a new draft as a reply to the last message in the conversation.
   *
   * @summary Create draft reply
   */
  createDraftReply(body: types.CreateDraftReplyBodyParam, metadata: types.CreateDraftReplyMetadataParam): Promise<FetchResponse<200, types.CreateDraftReplyResponse200>> {
    return this.core.fetch('/conversations/{conversation_id}/drafts', 'post', body, metadata);
  }

  /**
   * List the events that occured for a conversation in reverse chronological order (newest
   * first). The order will respect your company's [bump
   * settings](https://help.front.com/t/y729th/customize-when-conversations-bump-up), which
   * determine when conversations bump to the top.
   *
   * @summary List conversation events
   */
  listConversationEvents(metadata: types.ListConversationEventsMetadataParam): Promise<FetchResponse<200, types.ListConversationEventsResponse200>> {
    return this.core.fetch('/conversations/{conversation_id}/events', 'get', metadata);
  }

  /**
   * List the teammates following a conversation.
   *
   * @summary List conversation followers
   */
  listConversationFollowers(metadata: types.ListConversationFollowersMetadataParam): Promise<FetchResponse<200, types.ListConversationFollowersResponse200>> {
    return this.core.fetch('/conversations/{conversation_id}/followers', 'get', metadata);
  }

  /**
   * Adds teammates to the list of followers of a conversation.
   *
   * @summary Add conversation followers
   */
  addConversationFollowers(body: types.AddConversationFollowersBodyParam, metadata: types.AddConversationFollowersMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}/followers', 'post', body, metadata);
  }

  /**
   * Removes teammates from the list of followers of a conversation.
   *
   * @summary Delete conversation followers
   */
  deleteConversationFollowers(body: types.DeleteConversationFollowersBodyParam, metadata: types.DeleteConversationFollowersMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}/followers', 'delete', body, metadata);
  }

  /**
   * List the inboxes in which a conversation is listed.
   *
   * @summary List conversation inboxes
   */
  listConversationInboxes(metadata: types.ListConversationInboxesMetadataParam): Promise<FetchResponse<200, types.ListConversationInboxesResponse200>> {
    return this.core.fetch('/conversations/{conversation_id}/inboxes', 'get', metadata);
  }

  /**
   * Adds one or more links to a conversation
   *
   * @summary Add conversation link
   */
  addConversationLink(body: types.AddConversationLinkBodyParam, metadata: types.AddConversationLinkMetadataParam): Promise<FetchResponse<number, unknown>>;
  addConversationLink(metadata: types.AddConversationLinkMetadataParam): Promise<FetchResponse<number, unknown>>;
  addConversationLink(body?: types.AddConversationLinkBodyParam | types.AddConversationLinkMetadataParam, metadata?: types.AddConversationLinkMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}/links', 'post', body, metadata);
  }

  /**
   * Removes one or more links to a conversation
   *
   * @summary Remove conversation links
   */
  removeConversationLinks(body: types.RemoveConversationLinksBodyParam, metadata: types.RemoveConversationLinksMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}/links', 'delete', body, metadata);
  }

  /**
   * List the messages in a conversation in reverse chronological order (newest first).
   *
   * @summary List conversation messages
   */
  listConversationMessages(metadata: types.ListConversationMessagesMetadataParam): Promise<FetchResponse<200, types.ListConversationMessagesResponse200>> {
    return this.core.fetch('/conversations/{conversation_id}/messages', 'get', metadata);
  }

  /**
   * Reply to a conversation by sending a message and appending it to the conversation.
   *
   * @summary Create message reply
   */
  createMessageReply(body: types.CreateMessageReplyBodyParam, metadata: types.CreateMessageReplyMetadataParam): Promise<FetchResponse<202, types.CreateMessageReplyResponse202>> {
    return this.core.fetch('/conversations/{conversation_id}/messages', 'post', body, metadata);
  }

  /**
   * Snooze or unsnooze a conversation for the provided user.
   * For private conversations, reminders can only be created and edited through the API for
   * teammates that own the conversation.
   * For shared conversations, reminders created and edited through the API are shared for
   * all teammates within the shared inbox(es) that the conversation belongs to.
   *
   *
   * @summary Update conversation reminders
   */
  updateConversationReminders(body: types.UpdateConversationRemindersBodyParam, metadata: types.UpdateConversationRemindersMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}/reminders', 'patch', body, metadata);
  }

  /**
   * Adds one or more tags to a conversation
   *
   * @summary Add conversation tag
   */
  addConversationTag(body: types.AddConversationTagBodyParam, metadata: types.AddConversationTagMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}/tags', 'post', body, metadata);
  }

  /**
   * Removes one or more tags to a conversation
   *
   * @summary Remove conversation tag
   */
  removeConversationTag(body: types.RemoveConversationTagBodyParam, metadata: types.RemoveConversationTagMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/conversations/{conversation_id}/tags', 'delete', body, metadata);
  }

  /**
   * Lists the custom fields that can be attached to a Contact.
   *
   * > ⚠️ Deprecated endpoint
   * >
   * > This endpoint has been deprecated. Please use the fully compatible `GET
   * /contacts/custom_fields` endpoint instead.
   *
   *
   * @summary List Contact's custom fields
   */
  listCustomFields(): Promise<FetchResponse<200, types.ListCustomFieldsResponse200>> {
    return this.core.fetch('/custom_fields', 'get');
  }

  /**
   * Download an attachment file.
   *
   * @summary Download attachment
   */
  downloadAttachment(metadata: types.DownloadAttachmentMetadataParam): Promise<FetchResponse<200, types.DownloadAttachmentResponse200>> {
    return this.core.fetch('/download/{attachment_link_id}', 'get', metadata);
  }

  /**
   * Delete a draft message.
   *
   * @summary Delete draft
   */
  deleteDraft(body: types.DeleteDraftBodyParam, metadata: types.DeleteDraftMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/drafts/{draft_id}', 'delete', body, metadata);
  }

  /**
   * Edit a draft message.
   *
   * @summary Edit draft
   */
  editDraft(body: types.EditDraftBodyParam, metadata: types.EditDraftMetadataParam): Promise<FetchResponse<200, types.EditDraftResponse200>> {
    return this.core.fetch('/drafts/{message_id}/', 'patch', body, metadata);
  }

  /**
   * Lists all the detailed events which occured in the inboxes of the company ordered in
   * reverse chronological order (newest first).
   * Refer to the [Events](https://dev.frontapp.com/reference/events) topic for more
   * information, including sorting and filtering.
   * > ⚠️ Deprecated field may be included
   * >
   * > This route return the deprecated `last_message` conversation field for conversations
   * > associated with individual events. Please use the conversation's
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary List events
   */
  listEvents(metadata?: types.ListEventsMetadataParam): Promise<FetchResponse<200, types.ListEventsResponse200>> {
    return this.core.fetch('/events', 'get', metadata);
  }

  /**
   * Fetch an event.
   * Refer to the [Events](https://dev.frontapp.com/reference/events) topic for more
   * information, including sorting and filtering.
   * > ⚠️ Deprecated field may be included
   * >
   * > This route return the deprecated `last_message` conversation field for conversations
   * > associated with individual events. Please use the conversation's
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary Get event
   */
  getEvent(metadata: types.GetEventMetadataParam): Promise<FetchResponse<200, types.GetEventResponse200>> {
    return this.core.fetch('/events/{event_id}', 'get', metadata);
  }

  /**
   * List the inboxes of the company.
   *
   * @summary List inboxes
   */
  listInboxes(): Promise<FetchResponse<200, types.ListInboxesResponse200>> {
    return this.core.fetch('/inboxes', 'get');
  }

  /**
   * Create an inbox in the default team (workspace). The default team will be the oldest
   * team created that still exists at the time of the request.
   *
   * @summary Create inbox
   */
  createInbox(body: types.CreateInboxBodyParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/inboxes', 'post', body);
  }

  /**
   * Lists the custom fields that can be attached to an Inbox.
   *
   * @summary List Inbox's custom fields
   */
  listInboxCustomFields(): Promise<FetchResponse<200, types.ListInboxCustomFieldsResponse200>> {
    return this.core.fetch('/inboxes/custom_fields', 'get');
  }

  /**
   * Fetch an inbox.
   *
   * @summary Get inbox
   */
  getInbox(metadata: types.GetInboxMetadataParam): Promise<FetchResponse<200, types.GetInboxResponse200>> {
    return this.core.fetch('/inboxes/{inbox_id}', 'get', metadata);
  }

  /**
   * List the channels in an inbox.
   *
   * @summary List inbox channels
   */
  listInboxChannels(metadata: types.ListInboxChannelsMetadataParam): Promise<FetchResponse<200, types.ListInboxChannelsResponse200>> {
    return this.core.fetch('/inboxes/{inbox_id}/channels', 'get', metadata);
  }

  /**
   * Create a channel in an inbox.
   *
   * @summary Create a channel
   */
  createAChannel(body: types.CreateAChannelBodyParam, metadata: types.CreateAChannelMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/inboxes/{inbox_id}/channels', 'post', body, metadata);
  }

  /**
   * List the conversations in an inbox. For more advanced filtering, see the [search
   * endpoint](https://dev.frontapp.com/reference/conversations#search-conversations).
   * > ⚠️ Deprecated field included
   * >
   * > This endpoint returns a deprecated `last_message` field in the top-level conversation
   * bodies listed. Please use the
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary List inbox conversations
   */
  listInboxConversations(metadata: types.ListInboxConversationsMetadataParam): Promise<FetchResponse<200, types.ListInboxConversationsResponse200>> {
    return this.core.fetch('/inboxes/{inbox_id}/conversations', 'get', metadata);
  }

  /**
   * Import a new message in an inbox.
   *
   * @summary Import message
   */
  importInboxMessage(body: types.ImportInboxMessageBodyParam, metadata: types.ImportInboxMessageMetadataParam): Promise<FetchResponse<202, types.ImportInboxMessageResponse202>> {
    return this.core.fetch('/inboxes/{inbox_id}/imported_messages', 'post', body, metadata);
  }

  /**
   * List the teammates with access to an inbox.
   *
   * @summary List inbox access
   */
  listInboxAccess(metadata: types.ListInboxAccessMetadataParam): Promise<FetchResponse<200, types.ListInboxAccessResponse200>> {
    return this.core.fetch('/inboxes/{inbox_id}/teammates', 'get', metadata);
  }

  /**
   * Give access to one or more teammates to an inbox.
   *
   * @summary Add inbox access
   */
  addInboxAccess(body: types.AddInboxAccessBodyParam, metadata: types.AddInboxAccessMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/inboxes/{inbox_id}/teammates', 'post', body, metadata);
  }

  /**
   * Remove access of one or more teammates from an inbox.
   *
   * @summary Removes inbox access
   */
  removesInboxAccess(body: types.RemovesInboxAccessBodyParam, metadata: types.RemovesInboxAccessMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/inboxes/{inbox_id}/teammates', 'delete', body, metadata);
  }

  /**
   * List the links of the company paginated by id. Allows filtering by link type via the
   * q.types param.
   *
   * @summary List links
   */
  listLinks(metadata?: types.ListLinksMetadataParam): Promise<FetchResponse<200, types.ListLinksResponse200>> {
    return this.core.fetch('/links', 'get', metadata);
  }

  /**
   * Create a link. If the link is resolved to an installed links integration, any name
   * retrieved from the integration will override the provided name.
   *
   * @summary Create link
   */
  createLink(body: types.CreateLinkBodyParam): Promise<FetchResponse<201, types.CreateLinkResponse201>> {
    return this.core.fetch('/links', 'post', body);
  }

  /**
   * Lists the custom fields that can be attached to a Link.
   *
   * @summary List Link's custom fields
   */
  listLinkCustomFields(): Promise<FetchResponse<200, types.ListLinkCustomFieldsResponse200>> {
    return this.core.fetch('/links/custom_fields', 'get');
  }

  /**
   * Fetch a link.
   *
   * @summary Get link
   */
  getLink(metadata: types.GetLinkMetadataParam): Promise<FetchResponse<200, types.GetLinkResponse200>> {
    return this.core.fetch('/links/{link_id}', 'get', metadata);
  }

  /**
   * Update a link.
   *
   * @summary Update a link
   */
  updateALink(body: types.UpdateALinkBodyParam, metadata: types.UpdateALinkMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateALink(metadata: types.UpdateALinkMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateALink(body?: types.UpdateALinkBodyParam | types.UpdateALinkMetadataParam, metadata?: types.UpdateALinkMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/links/{link_id}', 'patch', body, metadata);
  }

  /**
   * List the conversations linked to a specific link. For more advanced filtering, see the
   * [search
   * endpoint](https://dev.frontapp.com/reference/conversations#search-conversations).
   * > ⚠️ Deprecated field included
   * >
   * > This endpoint returns a deprecated `last_message` field in the top-level conversation
   * bodies listed. Please use the
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary List link conversations
   */
  listLinkConversations(metadata: types.ListLinkConversationsMetadataParam): Promise<FetchResponse<200, types.ListLinkConversationsResponse200>> {
    return this.core.fetch('/links/{link_id}/conversations', 'get', metadata);
  }

  /**
   * Fetch the details of the API token.
   *
   * @summary API Token details
   */
  apiTokenDetails(): Promise<FetchResponse<200, types.ApiTokenDetailsResponse200>> {
    return this.core.fetch('/me', 'get');
  }

  /**
   * List the message template folders.
   *
   * @summary List folders
   */
  listFolders(metadata?: types.ListFoldersMetadataParam): Promise<FetchResponse<200, types.ListFoldersResponse200>> {
    return this.core.fetch('/message_template_folders', 'get', metadata);
  }

  /**
   * Create a new message template folder.
   *
   * @summary Create folder
   */
  createFolder(body: types.CreateFolderBodyParam): Promise<FetchResponse<201, types.CreateFolderResponse201>> {
    return this.core.fetch('/message_template_folders', 'post', body);
  }

  /**
   * Fetch a message template folder.
   *
   * @summary Get folder
   */
  getFolder(metadata: types.GetFolderMetadataParam): Promise<FetchResponse<200, types.GetFolderResponse200>> {
    return this.core.fetch('/message_template_folders/{message_template_folder_id}', 'get', metadata);
  }

  /**
   * Update message template folder
   *
   * @summary Update folder
   */
  updateFolder(body: types.UpdateFolderBodyParam, metadata: types.UpdateFolderMetadataParam): Promise<FetchResponse<200, types.UpdateFolderResponse200>>;
  updateFolder(metadata: types.UpdateFolderMetadataParam): Promise<FetchResponse<200, types.UpdateFolderResponse200>>;
  updateFolder(body?: types.UpdateFolderBodyParam | types.UpdateFolderMetadataParam, metadata?: types.UpdateFolderMetadataParam): Promise<FetchResponse<200, types.UpdateFolderResponse200>> {
    return this.core.fetch('/message_template_folders/{message_template_folder_id}', 'patch', body, metadata);
  }

  /**
   * Delete a message template folder and child folders/templates
   *
   * @summary Delete folder
   */
  deleteFolder(metadata: types.DeleteFolderMetadataParam): Promise<FetchResponse<202, types.DeleteFolderResponse202>> {
    return this.core.fetch('/message_template_folders/{message_template_folder_id}', 'delete', metadata);
  }

  /**
   * Fetch the child message templates folders of a message template folder.
   *
   * @summary Get child folders
   */
  getChildFolders(metadata: types.GetChildFoldersMetadataParam): Promise<FetchResponse<200, types.GetChildFoldersResponse200>> {
    return this.core.fetch('/message_template_folders/{message_template_folder_id}/message_template_folders', 'get', metadata);
  }

  /**
   * Create a new message template folder as a child of the given folder
   *
   * @summary Create child folder
   */
  createChildFolder(body: types.CreateChildFolderBodyParam, metadata: types.CreateChildFolderMetadataParam): Promise<FetchResponse<201, types.CreateChildFolderResponse201>> {
    return this.core.fetch('/message_template_folders/{message_template_folder_id}/message_template_folders', 'post', body, metadata);
  }

  /**
   * Fetch the child message templates of a message template folder.
   *
   * @summary Get child templates
   */
  getChildTemplates(metadata: types.GetChildTemplatesMetadataParam): Promise<FetchResponse<200, types.GetChildTemplatesResponse200>> {
    return this.core.fetch('/message_template_folders/{message_template_folder_id}/message_templates', 'get', metadata);
  }

  /**
   * Create a new message template as a child of the given folder
   *
   * @summary Create child template
   */
  createChildTemplate(body: types.CreateChildTemplateBodyParam, metadata: types.CreateChildTemplateMetadataParam): Promise<FetchResponse<201, types.CreateChildTemplateResponse201>> {
    return this.core.fetch('/message_template_folders/{message_template_folder_id}/message_templates', 'post', body, metadata);
  }

  /**
   * List the message templates.
   *
   * @summary List message templates
   */
  listMessageTemplates(metadata?: types.ListMessageTemplatesMetadataParam): Promise<FetchResponse<200, types.ListMessageTemplatesResponse200>> {
    return this.core.fetch('/message_templates', 'get', metadata);
  }

  /**
   * Create a new message template.
   *
   * @summary Create message template
   */
  createMessageTemplate(body: types.CreateMessageTemplateBodyParam): Promise<FetchResponse<201, types.CreateMessageTemplateResponse201>> {
    return this.core.fetch('/message_templates', 'post', body);
  }

  /**
   * Fetch a message template.
   *
   * @summary Get message template
   */
  getMessageTemplate(metadata: types.GetMessageTemplateMetadataParam): Promise<FetchResponse<200, types.GetMessageTemplateResponse200>> {
    return this.core.fetch('/message_templates/{message_template_id}', 'get', metadata);
  }

  /**
   * Update message template
   *
   * @summary Update message template
   */
  updateMessageTemplate(body: types.UpdateMessageTemplateBodyParam, metadata: types.UpdateMessageTemplateMetadataParam): Promise<FetchResponse<200, types.UpdateMessageTemplateResponse200>>;
  updateMessageTemplate(metadata: types.UpdateMessageTemplateMetadataParam): Promise<FetchResponse<200, types.UpdateMessageTemplateResponse200>>;
  updateMessageTemplate(body?: types.UpdateMessageTemplateBodyParam | types.UpdateMessageTemplateMetadataParam, metadata?: types.UpdateMessageTemplateMetadataParam): Promise<FetchResponse<200, types.UpdateMessageTemplateResponse200>> {
    return this.core.fetch('/message_templates/{message_template_id}', 'patch', body, metadata);
  }

  /**
   * Delete a message template
   *
   * @summary Delete message template
   */
  deleteMessageTemplate(metadata: types.DeleteMessageTemplateMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/message_templates/{message_template_id}', 'delete', metadata);
  }

  /**
   * Fetch a message.
   *
   * > ℹ️ The HTTP Header `Accept` can be used to request the message in a different format.
   * > By default, Front will return the documented JSON response. By requesting
   * `message/rfc822`, the response will contain the message in the EML format (for email
   * messages only).
   *
   *
   * @summary Get message
   */
  getMessage(metadata: types.GetMessageMetadataParam): Promise<FetchResponse<200, types.GetMessageResponse200>> {
    return this.core.fetch('/messages/{message_id}', 'get', metadata);
  }

  /**
   * Get the seen receipts for the given message. If no seen-by information is available,
   * there will be a single entry for the first time the message was seen by any recipient.
   * If seen-by information is available, there will be an entry for each recipient who has
   * seen the message.
   *
   * @summary Get message seen status
   */
  getMessageSeenStatus(metadata: types.GetMessageSeenStatusMetadataParam): Promise<FetchResponse<200, types.GetMessageSeenStatusResponse200>> {
    return this.core.fetch('/messages/{message_id}/seen', 'get', metadata);
  }

  /**
   * Mark an outbound message from Front as seen. Note, the message seen route should only be
   * called in response to an actual end-user's message-seen action. In accordance with this
   * behavior, the route is rate limited to 10 requests per hour. The request body should
   * send an empty object.
   *
   * @summary Mark message seen
   */
  markMessageSeen(body: types.MarkMessageSeenBodyParam, metadata: types.MarkMessageSeenMetadataParam): Promise<FetchResponse<number, unknown>>;
  markMessageSeen(metadata: types.MarkMessageSeenMetadataParam): Promise<FetchResponse<number, unknown>>;
  markMessageSeen(body?: types.MarkMessageSeenBodyParam | types.MarkMessageSeenMetadataParam, metadata?: types.MarkMessageSeenMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/messages/{message_id}/seen', 'post', body, metadata);
  }

  /**
   * List the rules of the company.
   *
   * @summary List rules
   */
  listRules(): Promise<FetchResponse<200, types.ListRulesResponse200>> {
    return this.core.fetch('/rules', 'get');
  }

  /**
   * Fetch a rule.
   *
   * @summary Get rule
   */
  getRule(metadata: types.GetRuleMetadataParam): Promise<FetchResponse<200, types.GetRuleResponse200>> {
    return this.core.fetch('/rules/{rule_id}', 'get', metadata);
  }

  /**
   * Fetch a shift.
   *
   * @summary Get shift
   */
  getShift(metadata: types.GetShiftMetadataParam): Promise<FetchResponse<200, types.GetShiftResponse200>> {
    return this.core.fetch('/shift/{shift_id}', 'get', metadata);
  }

  /**
   * List the teammates assigned to a shift.
   *
   * @summary List shift's teammates
   */
  listShiftsTeammates(metadata: types.ListShiftsTeammatesMetadataParam): Promise<FetchResponse<200, types.ListShiftsTeammatesResponse200>> {
    return this.core.fetch('/shift/{shift_id}/teammates', 'get', metadata);
  }

  /**
   * Add teammates to a shift. The selected teammates must be in the team that owns the
   * shift.
   *
   * @summary Add teammates to shift
   */
  addTeammatesToShift(body: types.AddTeammatesToShiftBodyParam, metadata: types.AddTeammatesToShiftMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/shift/{shift_id}/teammates', 'post', body, metadata);
  }

  /**
   * Remove teammates from a shift.
   *
   * @summary Remove teammates from shift
   */
  removeTeammatesFromShift(body: types.RemoveTeammatesFromShiftBodyParam, metadata: types.RemoveTeammatesFromShiftMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/shift/{shift_id}/teammates', 'delete', body, metadata);
  }

  /**
   * List the shifts.
   *
   * @summary List Shifts
   */
  listShifts(): Promise<FetchResponse<200, types.ListShiftsResponse200>> {
    return this.core.fetch('/shifts', 'get');
  }

  /**
   * Create a shift.
   *
   * @summary Create shift
   */
  createShift(body: types.CreateShiftBodyParam): Promise<FetchResponse<201, types.CreateShiftResponse201>> {
    return this.core.fetch('/shifts', 'post', body);
  }

  /**
   * Update a shift.
   *
   * @summary Update shift
   */
  updateShift(body: types.UpdateShiftBodyParam, metadata: types.UpdateShiftMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateShift(metadata: types.UpdateShiftMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateShift(body?: types.UpdateShiftBodyParam | types.UpdateShiftMetadataParam, metadata?: types.UpdateShiftMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/shifts/{shift_id}', 'patch', body, metadata);
  }

  /**
   * Get the given signature.
   *
   * @summary Get signatures
   */
  getSignatures(metadata: types.GetSignaturesMetadataParam): Promise<FetchResponse<200, types.GetSignaturesResponse200>> {
    return this.core.fetch('/signatures/{signature_id}', 'get', metadata);
  }

  /**
   * Update signature
   *
   * @summary Update signature
   */
  updateSignature(body: types.UpdateSignatureBodyParam, metadata: types.UpdateSignatureMetadataParam): Promise<FetchResponse<200, types.UpdateSignatureResponse200>>;
  updateSignature(metadata: types.UpdateSignatureMetadataParam): Promise<FetchResponse<200, types.UpdateSignatureResponse200>>;
  updateSignature(body?: types.UpdateSignatureBodyParam | types.UpdateSignatureMetadataParam, metadata?: types.UpdateSignatureMetadataParam): Promise<FetchResponse<200, types.UpdateSignatureResponse200>> {
    return this.core.fetch('/signatures/{signature_id}', 'patch', body, metadata);
  }

  /**
   * Delete signature
   *
   * @summary Delete signature
   */
  deleteSignature(metadata: types.DeleteSignatureMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/signatures/{signature_id}', 'delete', metadata);
  }

  /**
   * List all the tags of the company that the API token has access to, whether they be
   * company tags, team tags, or teammate tags.
   *
   * @summary List tags
   */
  listTags(metadata?: types.ListTagsMetadataParam): Promise<FetchResponse<200, types.ListTagsResponse200>> {
    return this.core.fetch('/tags', 'get', metadata);
  }

  /**
   * Create a tag in the oldest team (workspace). This is a legacy endpoint. Use the Create
   * company tag, Create team tag, or Create teammate tag endpoints instead.
   *
   * @summary Create tag
   */
  createTag(body: types.CreateTagBodyParam): Promise<FetchResponse<201, types.CreateTagResponse201>> {
    return this.core.fetch('/tags', 'post', body);
  }

  /**
   * Fetch a tag.
   *
   * @summary Get tag
   */
  getTag(metadata: types.GetTagMetadataParam): Promise<FetchResponse<200, types.GetTagResponse200>> {
    return this.core.fetch('/tags/{tag_id}', 'get', metadata);
  }

  /**
   * Update a tag.
   *
   * @summary Update a tag
   */
  updateATag(body: types.UpdateATagBodyParam, metadata: types.UpdateATagMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateATag(metadata: types.UpdateATagMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateATag(body?: types.UpdateATagBodyParam | types.UpdateATagMetadataParam, metadata?: types.UpdateATagMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/tags/{tag_id}', 'patch', body, metadata);
  }

  /**
   * Delete a tag.
   *
   * @summary Delete tag
   */
  deleteTag(metadata: types.DeleteTagMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/tags/{tag_id}', 'delete', metadata);
  }

  /**
   * List the children of a specific tag.
   *
   * @summary List tag children
   */
  listTagChildren(metadata: types.ListTagChildrenMetadataParam): Promise<FetchResponse<200, types.ListTagChildrenResponse200>> {
    return this.core.fetch('/tags/{tag_id}/children', 'get', metadata);
  }

  /**
   * Creates a child tag.
   *
   * @summary Create child tag
   */
  createChildTag(body: types.CreateChildTagBodyParam, metadata: types.CreateChildTagMetadataParam): Promise<FetchResponse<201, types.CreateChildTagResponse201>> {
    return this.core.fetch('/tags/{tag_id}/children', 'post', body, metadata);
  }

  /**
   * List the conversations tagged with a tag. For more advanced filtering, see the [search
   * endpoint](https://dev.frontapp.com/reference/conversations#search-conversations).
   * > ⚠️ Deprecated field included
   * >
   * > This endpoint returns a deprecated `last_message` field in the top-level conversation
   * bodies listed. Please use the
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary List tagged conversations
   */
  listTaggedConversations(metadata: types.ListTaggedConversationsMetadataParam): Promise<FetchResponse<200, types.ListTaggedConversationsResponse200>> {
    return this.core.fetch('/tags/{tag_id}/conversations', 'get', metadata);
  }

  /**
   * List the teammates in the company.
   *
   * @summary List teammates
   */
  listTeammates(): Promise<FetchResponse<200, types.ListTeammatesResponse200>> {
    return this.core.fetch('/teammates', 'get');
  }

  /**
   * Lists the custom fields that can be attached to a Teammate.
   *
   * @summary List Teammate's custom fields
   */
  listTeammateCustomFields(): Promise<FetchResponse<200, types.ListTeammateCustomFieldsResponse200>> {
    return this.core.fetch('/teammates/custom_fields', 'get');
  }

  /**
   * Fetch a teammate.
   *
   * @summary Get teammate
   */
  getTeammate(metadata: types.GetTeammateMetadataParam): Promise<FetchResponse<200, types.GetTeammateResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}', 'get', metadata);
  }

  /**
   * Update a teammate.
   *
   * @summary Update teammate
   */
  updateTeammate(body: types.UpdateTeammateBodyParam, metadata: types.UpdateTeammateMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateTeammate(metadata: types.UpdateTeammateMetadataParam): Promise<FetchResponse<number, unknown>>;
  updateTeammate(body?: types.UpdateTeammateBodyParam | types.UpdateTeammateMetadataParam, metadata?: types.UpdateTeammateMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/teammates/{teammate_id}', 'patch', body, metadata);
  }

  /**
   * List the channels of a teammate.
   *
   * @summary List teammate channels
   */
  listTeammateChannels(metadata: types.ListTeammateChannelsMetadataParam): Promise<FetchResponse<200, types.ListTeammateChannelsResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/channels', 'get', metadata);
  }

  /**
   * List the contact groups belonging to the requested teammate.
   *
   * @summary List teammate groups
   */
  listTeammateGroups(metadata: types.ListTeammateGroupsMetadataParam): Promise<FetchResponse<200, types.ListTeammateGroupsResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/contact_groups', 'get', metadata);
  }

  /**
   * Create a new contact group for the requested teammate.
   *
   * @summary Create teammate group
   */
  createTeammateGroup(body: types.CreateTeammateGroupBodyParam, metadata: types.CreateTeammateGroupMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/teammates/{teammate_id}/contact_groups', 'post', body, metadata);
  }

  /**
   * List the contacts of a teammate.
   *
   * @summary List teammate contacts
   */
  listTeammateContacts(metadata: types.ListTeammateContactsMetadataParam): Promise<FetchResponse<200, types.ListTeammateContactsResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/contacts', 'get', metadata);
  }

  /**
   * Create a contact for a teammate.
   *
   * @summary Create teammate contact
   */
  createTeammateContact(body: types.CreateTeammateContactBodyParam, metadata: types.CreateTeammateContactMetadataParam): Promise<FetchResponse<201, types.CreateTeammateContactResponse201>> {
    return this.core.fetch('/teammates/{teammate_id}/contacts', 'post', body, metadata);
  }

  /**
   * List the conversations assigned to a teammate in reverse chronological order (most
   * recently updated first). For more advanced filtering, see the [search
   * endpoint](https://dev.frontapp.com/reference/conversations#search-conversations).
   * > ⚠️ Deprecated field included
   * >
   * > This endpoint returns a deprecated `last_message` field in the top-level conversation
   * bodies listed. Please use the
   * > `_links.related.last_message` field instead.
   *
   *
   * @summary List assigned conversations
   */
  listAssignedConversations(metadata: types.ListAssignedConversationsMetadataParam): Promise<FetchResponse<200, types.ListAssignedConversationsResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/conversations', 'get', metadata);
  }

  /**
   * Returns list of inboxes the teammate has access to.
   *
   * @summary List teammate inboxes
   */
  listTeammateInboxes(metadata: types.ListTeammateInboxesMetadataParam): Promise<FetchResponse<200, types.ListTeammateInboxesResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/inboxes', 'get', metadata);
  }

  /**
   * List the message template folders belonging to the requested teammate.
   *
   * @summary List teammate folders
   */
  listTeammateFolders(metadata: types.ListTeammateFoldersMetadataParam): Promise<FetchResponse<200, types.ListTeammateFoldersResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/message_template_folders', 'get', metadata);
  }

  /**
   * Create a new message template folder belonging to the requested teammate.
   *
   * @summary Create teammate folder
   */
  createTeammateFolder(body: types.CreateTeammateFolderBodyParam, metadata: types.CreateTeammateFolderMetadataParam): Promise<FetchResponse<201, types.CreateTeammateFolderResponse201>> {
    return this.core.fetch('/teammates/{teammate_id}/message_template_folders', 'post', body, metadata);
  }

  /**
   * List the message templates belonging to the requested teammate.
   *
   * @summary List teammate message templates
   */
  listTeammateMessageTemplates(metadata: types.ListTeammateMessageTemplatesMetadataParam): Promise<FetchResponse<200, types.ListTeammateMessageTemplatesResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/message_templates', 'get', metadata);
  }

  /**
   * Create a new message template for the given teammate
   *
   * @summary Create teammate message template
   */
  createTeammateMessageTemplate(body: types.CreateTeammateMessageTemplateBodyParam, metadata: types.CreateTeammateMessageTemplateMetadataParam): Promise<FetchResponse<201, types.CreateTeammateMessageTemplateResponse201>> {
    return this.core.fetch('/teammates/{teammate_id}/message_templates', 'post', body, metadata);
  }

  /**
   * List the rules of a teammate.
   *
   * @summary List teammate rules
   */
  listTeammateRules(metadata: types.ListTeammateRulesMetadataParam): Promise<FetchResponse<200, types.ListTeammateRulesResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/rules', 'get', metadata);
  }

  /**
   * Lists all the shifts for the teammate.
   *
   * @summary List Teammate Shifts
   */
  listTeammateShifts(metadata: types.ListTeammateShiftsMetadataParam): Promise<FetchResponse<200, types.ListTeammateShiftsResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/shifts', 'get', metadata);
  }

  /**
   * List the signatures belonging to the given teammate.
   *
   * @summary List teammate signatures
   */
  listTeammateSignatures(metadata: types.ListTeammateSignaturesMetadataParam): Promise<FetchResponse<200, types.ListTeammateSignaturesResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/signatures', 'get', metadata);
  }

  /**
   * Create a new signature for the given teammate
   *
   * @summary Create teammate signature
   */
  createTeammateSignature(body: types.CreateTeammateSignatureBodyParam, metadata: types.CreateTeammateSignatureMetadataParam): Promise<FetchResponse<201, types.CreateTeammateSignatureResponse201>> {
    return this.core.fetch('/teammates/{teammate_id}/signatures', 'post', body, metadata);
  }

  /**
   * List the tags for a teammate.
   *
   * @summary List teammate tags
   */
  listTeammateTags(metadata: types.ListTeammateTagsMetadataParam): Promise<FetchResponse<200, types.ListTeammateTagsResponse200>> {
    return this.core.fetch('/teammates/{teammate_id}/tags', 'get', metadata);
  }

  /**
   * Create a tag for a teammate.
   *
   * @summary Create teammate tag
   */
  createTeammateTag(body: types.CreateTeammateTagBodyParam, metadata: types.CreateTeammateTagMetadataParam): Promise<FetchResponse<201, types.CreateTeammateTagResponse201>> {
    return this.core.fetch('/teammates/{teammate_id}/tags', 'post', body, metadata);
  }

  /**
   * List the teams (workspaces) in the company.
   *
   * @summary List teams
   */
  listTeams(): Promise<FetchResponse<200, types.ListTeamsResponse200>> {
    return this.core.fetch('/teams', 'get');
  }

  /**
   * Fetch a team (workspace).
   *
   * @summary Get team
   */
  getTeam(metadata: types.GetTeamMetadataParam): Promise<FetchResponse<200, types.GetTeamResponse200>> {
    return this.core.fetch('/teams/{team_id}', 'get', metadata);
  }

  /**
   * List the channels of a team (workspace).
   *
   * @summary List team channels
   */
  listTeamChannels(metadata: types.ListTeamChannelsMetadataParam): Promise<FetchResponse<200, types.ListTeamChannelsResponse200>> {
    return this.core.fetch('/teams/{team_id}/channels', 'get', metadata);
  }

  /**
   * List contact groups belonging to the requested team (workspace).
   *
   * @summary List team groups
   */
  listTeamGroups(metadata: types.ListTeamGroupsMetadataParam): Promise<FetchResponse<200, types.ListTeamGroupsResponse200>> {
    return this.core.fetch('/teams/{team_id}/contact_groups', 'get', metadata);
  }

  /**
   * Create a new contact group for the requested team (workspace).
   *
   * @summary Create team group
   */
  createTeamGroup(body: types.CreateTeamGroupBodyParam, metadata: types.CreateTeamGroupMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/teams/{team_id}/contact_groups', 'post', body, metadata);
  }

  /**
   * List the contacts of a team (workspace).
   *
   * @summary List team contacts
   */
  listTeamContacts(metadata: types.ListTeamContactsMetadataParam): Promise<FetchResponse<200, types.ListTeamContactsResponse200>> {
    return this.core.fetch('/teams/{team_id}/contacts', 'get', metadata);
  }

  /**
   * Create a contact for a team (workspace).
   *
   * @summary Create team contact
   */
  createTeamContact(body: types.CreateTeamContactBodyParam, metadata: types.CreateTeamContactMetadataParam): Promise<FetchResponse<201, types.CreateTeamContactResponse201>> {
    return this.core.fetch('/teams/{team_id}/contacts', 'post', body, metadata);
  }

  /**
   * List the inboxes belonging to a team (workspace).
   *
   * @summary List team inboxes
   */
  listTeamInboxes(metadata: types.ListTeamInboxesMetadataParam): Promise<FetchResponse<200, types.ListTeamInboxesResponse200>> {
    return this.core.fetch('/teams/{team_id}/inboxes', 'get', metadata);
  }

  /**
   * Create an inbox for a team (workspace).
   *
   * @summary Create team inbox
   */
  createTeamInbox(body: types.CreateTeamInboxBodyParam, metadata: types.CreateTeamInboxMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/teams/{team_id}/inboxes', 'post', body, metadata);
  }

  /**
   * List the message template folders belonging to the requested team (workspace).
   *
   * @summary List team folders
   */
  listTeamFolders(metadata: types.ListTeamFoldersMetadataParam): Promise<FetchResponse<200, types.ListTeamFoldersResponse200>> {
    return this.core.fetch('/teams/{team_id}/message_template_folders', 'get', metadata);
  }

  /**
   * Create a new message template folder belonging to the requested team (workspace).
   *
   * @summary Create team folder
   */
  createTeamFolder(body: types.CreateTeamFolderBodyParam, metadata: types.CreateTeamFolderMetadataParam): Promise<FetchResponse<201, types.CreateTeamFolderResponse201>> {
    return this.core.fetch('/teams/{team_id}/message_template_folders', 'post', body, metadata);
  }

  /**
   * List the message templates belonging to the requested team (workspace).
   *
   * @summary List team message templates
   */
  listTeamMessageTemplates(metadata: types.ListTeamMessageTemplatesMetadataParam): Promise<FetchResponse<200, types.ListTeamMessageTemplatesResponse200>> {
    return this.core.fetch('/teams/{team_id}/message_templates', 'get', metadata);
  }

  /**
   * Create a new message template for the given team (workspace).
   *
   * @summary Create team message template
   */
  createTeamMessageTemplate(body: types.CreateTeamMessageTemplateBodyParam, metadata: types.CreateTeamMessageTemplateMetadataParam): Promise<FetchResponse<201, types.CreateTeamMessageTemplateResponse201>> {
    return this.core.fetch('/teams/{team_id}/message_templates', 'post', body, metadata);
  }

  /**
   * List the rules of a team (workspace).
   *
   * @summary List team rules
   */
  listTeamRules(metadata: types.ListTeamRulesMetadataParam): Promise<FetchResponse<200, types.ListTeamRulesResponse200>> {
    return this.core.fetch('/teams/{team_id}/rules', 'get', metadata);
  }

  /**
   * List the shifts for a team (workspace).
   *
   * @summary List team Shifts
   */
  listTeamShifts(metadata: types.ListTeamShiftsMetadataParam): Promise<FetchResponse<200, types.ListTeamShiftsResponse200>> {
    return this.core.fetch('/teams/{team_id}/shifts', 'get', metadata);
  }

  /**
   * Create a shift for a team (workspace).
   *
   * @summary Create team shift
   */
  createTeamShift(body: types.CreateTeamShiftBodyParam, metadata: types.CreateTeamShiftMetadataParam): Promise<FetchResponse<201, types.CreateTeamShiftResponse201>> {
    return this.core.fetch('/teams/{team_id}/shifts', 'post', body, metadata);
  }

  /**
   * List the signatures belonging to the given team (workspace).
   *
   * @summary List team signatures
   */
  listTeamSignatures(metadata: types.ListTeamSignaturesMetadataParam): Promise<FetchResponse<200, types.ListTeamSignaturesResponse200>> {
    return this.core.fetch('/teams/{team_id}/signatures', 'get', metadata);
  }

  /**
   * Create a new signature for the given team (workspace).
   *
   * @summary Create team signature
   */
  createTeamSignature(body: types.CreateTeamSignatureBodyParam, metadata: types.CreateTeamSignatureMetadataParam): Promise<FetchResponse<201, types.CreateTeamSignatureResponse201>> {
    return this.core.fetch('/teams/{team_id}/signatures', 'post', body, metadata);
  }

  /**
   * List the tags for a team (workspace).
   *
   * @summary List team tags
   */
  listTeamTags(metadata: types.ListTeamTagsMetadataParam): Promise<FetchResponse<200, types.ListTeamTagsResponse200>> {
    return this.core.fetch('/teams/{team_id}/tags', 'get', metadata);
  }

  /**
   * Create a tag for a team (workspace).
   *
   * @summary Create team tag
   */
  createTeamTag(body: types.CreateTeamTagBodyParam, metadata: types.CreateTeamTagMetadataParam): Promise<FetchResponse<201, types.CreateTeamTagResponse201>> {
    return this.core.fetch('/teams/{team_id}/tags', 'post', body, metadata);
  }

  /**
   * Add one or more teammates to a team (workspace).
   *
   * @summary Add teammates to team
   */
  addTeammatesToTeam(body: types.AddTeammatesToTeamBodyParam, metadata: types.AddTeammatesToTeamMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/teams/{team_id}/teammates', 'post', body, metadata);
  }

  /**
   * Remove one or more teammates from a team (workspace). Alternatively, you can supply
   * emails as a [resource alias](https://dev.frontapp.com/docs/resource-aliases-1).
   *
   * @summary Remove teammates from team
   */
  removeTeammatesFromTeam(body: types.RemoveTeammatesFromTeamBodyParam, metadata: types.RemoveTeammatesFromTeamMetadataParam): Promise<FetchResponse<number, unknown>> {
    return this.core.fetch('/teams/{team_id}/teammates', 'delete', body, metadata);
  }
}

const createSDK = (() => { return new SDK(); })()
;

export default createSDK;

export type { AddCommentBodyParam, AddCommentMetadataParam, AddCommentResponse201, AddContactHandleBodyParam, AddContactHandleMetadataParam, AddContactToAccountBodyParam, AddContactToAccountMetadataParam, AddContactsToGroupBodyParam, AddContactsToGroupMetadataParam, AddConversationFollowersBodyParam, AddConversationFollowersMetadataParam, AddConversationLinkBodyParam, AddConversationLinkMetadataParam, AddConversationTagBodyParam, AddConversationTagMetadataParam, AddInboxAccessBodyParam, AddInboxAccessMetadataParam, AddNoteBodyParam, AddNoteMetadataParam, AddNoteResponse201, AddTeammatesToShiftBodyParam, AddTeammatesToShiftMetadataParam, AddTeammatesToTeamBodyParam, AddTeammatesToTeamMetadataParam, ApiTokenDetailsResponse200, CreateAChannelBodyParam, CreateAChannelMetadataParam, CreateAccountBodyParam, CreateAccountResponse201, CreateAnalyticsExportBodyParam, CreateAnalyticsExportResponse201, CreateAnalyticsReportBodyParam, CreateAnalyticsReportResponse201, CreateChildFolderBodyParam, CreateChildFolderMetadataParam, CreateChildFolderResponse201, CreateChildTagBodyParam, CreateChildTagMetadataParam, CreateChildTagResponse201, CreateChildTemplateBodyParam, CreateChildTemplateMetadataParam, CreateChildTemplateResponse201, CreateCompanyTagBodyParam, CreateCompanyTagResponse201, CreateContactBodyParam, CreateContactResponse201, CreateConversationBodyParam, CreateConversationResponse201, CreateDraftBodyParam, CreateDraftMetadataParam, CreateDraftReplyBodyParam, CreateDraftReplyMetadataParam, CreateDraftReplyResponse200, CreateDraftResponse200, CreateFolderBodyParam, CreateFolderResponse201, CreateGroupBodyParam, CreateInboxBodyParam, CreateLinkBodyParam, CreateLinkResponse201, CreateMessageBodyParam, CreateMessageMetadataParam, CreateMessageReplyBodyParam, CreateMessageReplyMetadataParam, CreateMessageReplyResponse202, CreateMessageResponse202, CreateMessageTemplateBodyParam, CreateMessageTemplateResponse201, CreateShiftBodyParam, CreateShiftResponse201, CreateTagBodyParam, CreateTagResponse201, CreateTeamContactBodyParam, CreateTeamContactMetadataParam, CreateTeamContactResponse201, CreateTeamFolderBodyParam, CreateTeamFolderMetadataParam, CreateTeamFolderResponse201, CreateTeamGroupBodyParam, CreateTeamGroupMetadataParam, CreateTeamInboxBodyParam, CreateTeamInboxMetadataParam, CreateTeamMessageTemplateBodyParam, CreateTeamMessageTemplateMetadataParam, CreateTeamMessageTemplateResponse201, CreateTeamShiftBodyParam, CreateTeamShiftMetadataParam, CreateTeamShiftResponse201, CreateTeamSignatureBodyParam, CreateTeamSignatureMetadataParam, CreateTeamSignatureResponse201, CreateTeamTagBodyParam, CreateTeamTagMetadataParam, CreateTeamTagResponse201, CreateTeammateContactBodyParam, CreateTeammateContactMetadataParam, CreateTeammateContactResponse201, CreateTeammateFolderBodyParam, CreateTeammateFolderMetadataParam, CreateTeammateFolderResponse201, CreateTeammateGroupBodyParam, CreateTeammateGroupMetadataParam, CreateTeammateMessageTemplateBodyParam, CreateTeammateMessageTemplateMetadataParam, CreateTeammateMessageTemplateResponse201, CreateTeammateSignatureBodyParam, CreateTeammateSignatureMetadataParam, CreateTeammateSignatureResponse201, CreateTeammateTagBodyParam, CreateTeammateTagMetadataParam, CreateTeammateTagResponse201, DeleteAContactMetadataParam, DeleteAnAccountMetadataParam, DeleteContactHandleBodyParam, DeleteContactHandleMetadataParam, DeleteConversationFollowersBodyParam, DeleteConversationFollowersMetadataParam, DeleteDraftBodyParam, DeleteDraftMetadataParam, DeleteFolderMetadataParam, DeleteFolderResponse202, DeleteGroupMetadataParam, DeleteMessageTemplateMetadataParam, DeleteSignatureMetadataParam, DeleteTagMetadataParam, DownloadAttachmentMetadataParam, DownloadAttachmentResponse200, EditDraftBodyParam, EditDraftMetadataParam, EditDraftResponse200, FetchAnAccountMetadataParam, FetchAnAccountResponse200, GetAnalyticsExportMetadataParam, GetAnalyticsExportResponse200, GetAnalyticsReportMetadataParam, GetAnalyticsReportResponse200, GetChannelMetadataParam, GetChannelResponse200, GetChildFoldersMetadataParam, GetChildFoldersResponse200, GetChildTemplatesMetadataParam, GetChildTemplatesResponse200, GetCommentMetadataParam, GetCommentResponse200, GetContactMetadataParam, GetContactResponse200, GetConversationByIdMetadataParam, GetConversationByIdResponse200, GetEventMetadataParam, GetEventResponse200, GetFolderMetadataParam, GetFolderResponse200, GetInboxMetadataParam, GetInboxResponse200, GetLinkMetadataParam, GetLinkResponse200, GetMessageMetadataParam, GetMessageResponse200, GetMessageSeenStatusMetadataParam, GetMessageSeenStatusResponse200, GetMessageTemplateMetadataParam, GetMessageTemplateResponse200, GetRuleMetadataParam, GetRuleResponse200, GetShiftMetadataParam, GetShiftResponse200, GetSignaturesMetadataParam, GetSignaturesResponse200, GetTagMetadataParam, GetTagResponse200, GetTeamMetadataParam, GetTeamResponse200, GetTeammateMetadataParam, GetTeammateResponse200, ImportInboxMessageBodyParam, ImportInboxMessageMetadataParam, ImportInboxMessageResponse202, ListAccountContactsMetadataParam, ListAccountContactsResponse200, ListAccountCustomFieldsResponse200, ListAccountsMetadataParam, ListAccountsResponse200, ListAllCompanyRulesResponse200, ListAssignedConversationsMetadataParam, ListAssignedConversationsResponse200, ListChannelsResponse200, ListCommentMentionsMetadataParam, ListCommentMentionsResponse200, ListCompanyTagsMetadataParam, ListCompanyTagsResponse200, ListContactConversationsMetadataParam, ListContactConversationsResponse200, ListContactCustomFieldsResponse200, ListContactsInGroupMetadataParam, ListContactsInGroupResponse200, ListContactsMetadataParam, ListContactsResponse200, ListConversationCommentsMetadataParam, ListConversationCommentsResponse200, ListConversationCustomFieldsResponse200, ListConversationDraftsMetadataParam, ListConversationDraftsResponse200, ListConversationEventsMetadataParam, ListConversationEventsResponse200, ListConversationFollowersMetadataParam, ListConversationFollowersResponse200, ListConversationInboxesMetadataParam, ListConversationInboxesResponse200, ListConversationMessagesMetadataParam, ListConversationMessagesResponse200, ListConversationsMetadataParam, ListConversationsResponse200, ListCustomFieldsResponse200, ListEventsMetadataParam, ListEventsResponse200, ListFoldersMetadataParam, ListFoldersResponse200, ListGroupsResponse200, ListInboxAccessMetadataParam, ListInboxAccessResponse200, ListInboxChannelsMetadataParam, ListInboxChannelsResponse200, ListInboxConversationsMetadataParam, ListInboxConversationsResponse200, ListInboxCustomFieldsResponse200, ListInboxesResponse200, ListLinkConversationsMetadataParam, ListLinkConversationsResponse200, ListLinkCustomFieldsResponse200, ListLinksMetadataParam, ListLinksResponse200, ListMessageTemplatesMetadataParam, ListMessageTemplatesResponse200, ListNotesMetadataParam, ListNotesResponse202, ListRulesResponse200, ListShiftsResponse200, ListShiftsTeammatesMetadataParam, ListShiftsTeammatesResponse200, ListTagChildrenMetadataParam, ListTagChildrenResponse200, ListTaggedConversationsMetadataParam, ListTaggedConversationsResponse200, ListTagsMetadataParam, ListTagsResponse200, ListTeamChannelsMetadataParam, ListTeamChannelsResponse200, ListTeamContactsMetadataParam, ListTeamContactsResponse200, ListTeamFoldersMetadataParam, ListTeamFoldersResponse200, ListTeamGroupsMetadataParam, ListTeamGroupsResponse200, ListTeamInboxesMetadataParam, ListTeamInboxesResponse200, ListTeamMessageTemplatesMetadataParam, ListTeamMessageTemplatesResponse200, ListTeamRulesMetadataParam, ListTeamRulesResponse200, ListTeamShiftsMetadataParam, ListTeamShiftsResponse200, ListTeamSignaturesMetadataParam, ListTeamSignaturesResponse200, ListTeamTagsMetadataParam, ListTeamTagsResponse200, ListTeammateChannelsMetadataParam, ListTeammateChannelsResponse200, ListTeammateContactsMetadataParam, ListTeammateContactsResponse200, ListTeammateCustomFieldsResponse200, ListTeammateFoldersMetadataParam, ListTeammateFoldersResponse200, ListTeammateGroupsMetadataParam, ListTeammateGroupsResponse200, ListTeammateInboxesMetadataParam, ListTeammateInboxesResponse200, ListTeammateMessageTemplatesMetadataParam, ListTeammateMessageTemplatesResponse200, ListTeammateRulesMetadataParam, ListTeammateRulesResponse200, ListTeammateShiftsMetadataParam, ListTeammateShiftsResponse200, ListTeammateSignaturesMetadataParam, ListTeammateSignaturesResponse200, ListTeammateTagsMetadataParam, ListTeammateTagsResponse200, ListTeammatesResponse200, ListTeamsResponse200, MarkMessageSeenBodyParam, MarkMessageSeenMetadataParam, MergeContactsBodyParam, MergeContactsResponse200, ReceiveCustomMessagesBodyParam, ReceiveCustomMessagesMetadataParam, ReceiveCustomMessagesResponse202, RemoveContactFromAccountBodyParam, RemoveContactFromAccountMetadataParam, RemoveContactsFromGroupBodyParam, RemoveContactsFromGroupMetadataParam, RemoveConversationLinksBodyParam, RemoveConversationLinksMetadataParam, RemoveConversationTagBodyParam, RemoveConversationTagMetadataParam, RemoveTeammatesFromShiftBodyParam, RemoveTeammatesFromShiftMetadataParam, RemoveTeammatesFromTeamBodyParam, RemoveTeammatesFromTeamMetadataParam, RemovesInboxAccessBodyParam, RemovesInboxAccessMetadataParam, SearchConversationsMetadataParam, SearchConversationsResponse200, UpdateAContactBodyParam, UpdateAContactMetadataParam, UpdateALinkBodyParam, UpdateALinkMetadataParam, UpdateATagBodyParam, UpdateATagMetadataParam, UpdateAccountBodyParam, UpdateAccountMetadataParam, UpdateAccountResponse200, UpdateChannelBodyParam, UpdateChannelMetadataParam, UpdateConversationAssigneeBodyParam, UpdateConversationAssigneeMetadataParam, UpdateConversationBodyParam, UpdateConversationMetadataParam, UpdateConversationRemindersBodyParam, UpdateConversationRemindersMetadataParam, UpdateFolderBodyParam, UpdateFolderMetadataParam, UpdateFolderResponse200, UpdateMessageTemplateBodyParam, UpdateMessageTemplateMetadataParam, UpdateMessageTemplateResponse200, UpdateShiftBodyParam, UpdateShiftMetadataParam, UpdateSignatureBodyParam, UpdateSignatureMetadataParam, UpdateSignatureResponse200, UpdateTeammateBodyParam, UpdateTeammateMetadataParam, ValidateChannelMetadataParam, ValidateChannelResponse202 } from './types';
