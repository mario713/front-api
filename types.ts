import type { FromSchema } from 'json-schema-to-ts';
import * as schemas from './schemas';

export type AddCommentBodyParam = FromSchema<typeof schemas.AddComment.body>;
export type AddCommentMetadataParam = FromSchema<typeof schemas.AddComment.metadata>;
export type AddCommentResponse201 = FromSchema<typeof schemas.AddComment.response['201']>;
export type AddContactHandleBodyParam = FromSchema<typeof schemas.AddContactHandle.body>;
export type AddContactHandleMetadataParam = FromSchema<typeof schemas.AddContactHandle.metadata>;
export type AddContactToAccountBodyParam = FromSchema<typeof schemas.AddContactToAccount.body>;
export type AddContactToAccountMetadataParam = FromSchema<typeof schemas.AddContactToAccount.metadata>;
export type AddContactsToGroupBodyParam = FromSchema<typeof schemas.AddContactsToGroup.body>;
export type AddContactsToGroupMetadataParam = FromSchema<typeof schemas.AddContactsToGroup.metadata>;
export type AddConversationFollowersBodyParam = FromSchema<typeof schemas.AddConversationFollowers.body>;
export type AddConversationFollowersMetadataParam = FromSchema<typeof schemas.AddConversationFollowers.metadata>;
export type AddConversationLinkBodyParam = FromSchema<typeof schemas.AddConversationLink.body>;
export type AddConversationLinkMetadataParam = FromSchema<typeof schemas.AddConversationLink.metadata>;
export type AddConversationTagBodyParam = FromSchema<typeof schemas.AddConversationTag.body>;
export type AddConversationTagMetadataParam = FromSchema<typeof schemas.AddConversationTag.metadata>;
export type AddInboxAccessBodyParam = FromSchema<typeof schemas.AddInboxAccess.body>;
export type AddInboxAccessMetadataParam = FromSchema<typeof schemas.AddInboxAccess.metadata>;
export type AddNoteBodyParam = FromSchema<typeof schemas.AddNote.body>;
export type AddNoteMetadataParam = FromSchema<typeof schemas.AddNote.metadata>;
export type AddNoteResponse201 = FromSchema<typeof schemas.AddNote.response['201']>;
export type AddTeammatesToShiftBodyParam = FromSchema<typeof schemas.AddTeammatesToShift.body>;
export type AddTeammatesToShiftMetadataParam = FromSchema<typeof schemas.AddTeammatesToShift.metadata>;
export type AddTeammatesToTeamBodyParam = FromSchema<typeof schemas.AddTeammatesToTeam.body>;
export type AddTeammatesToTeamMetadataParam = FromSchema<typeof schemas.AddTeammatesToTeam.metadata>;
export type ApiTokenDetailsResponse200 = FromSchema<typeof schemas.ApiTokenDetails.response['200']>;
export type CreateAChannelBodyParam = FromSchema<typeof schemas.CreateAChannel.body>;
export type CreateAChannelMetadataParam = FromSchema<typeof schemas.CreateAChannel.metadata>;
export type CreateAccountBodyParam = FromSchema<typeof schemas.CreateAccount.body>;
export type CreateAccountResponse201 = FromSchema<typeof schemas.CreateAccount.response['201']>;
export type CreateAnalyticsExportBodyParam = FromSchema<typeof schemas.CreateAnalyticsExport.body>;
export type CreateAnalyticsExportResponse201 = FromSchema<typeof schemas.CreateAnalyticsExport.response['201']>;
export type CreateAnalyticsReportBodyParam = FromSchema<typeof schemas.CreateAnalyticsReport.body>;
export type CreateAnalyticsReportResponse201 = FromSchema<typeof schemas.CreateAnalyticsReport.response['201']>;
export type CreateChildFolderBodyParam = FromSchema<typeof schemas.CreateChildFolder.body>;
export type CreateChildFolderMetadataParam = FromSchema<typeof schemas.CreateChildFolder.metadata>;
export type CreateChildFolderResponse201 = FromSchema<typeof schemas.CreateChildFolder.response['201']>;
export type CreateChildTagBodyParam = FromSchema<typeof schemas.CreateChildTag.body>;
export type CreateChildTagMetadataParam = FromSchema<typeof schemas.CreateChildTag.metadata>;
export type CreateChildTagResponse201 = FromSchema<typeof schemas.CreateChildTag.response['201']>;
export type CreateChildTemplateBodyParam = FromSchema<typeof schemas.CreateChildTemplate.body>;
export type CreateChildTemplateMetadataParam = FromSchema<typeof schemas.CreateChildTemplate.metadata>;
export type CreateChildTemplateResponse201 = FromSchema<typeof schemas.CreateChildTemplate.response['201']>;
export type CreateCompanyTagBodyParam = FromSchema<typeof schemas.CreateCompanyTag.body>;
export type CreateCompanyTagResponse201 = FromSchema<typeof schemas.CreateCompanyTag.response['201']>;
export type CreateContactBodyParam = FromSchema<typeof schemas.CreateContact.body>;
export type CreateContactResponse201 = FromSchema<typeof schemas.CreateContact.response['201']>;
export type CreateConversationBodyParam = FromSchema<typeof schemas.CreateConversation.body>;
export type CreateConversationResponse201 = FromSchema<typeof schemas.CreateConversation.response['201']>;
export type CreateDraftBodyParam = FromSchema<typeof schemas.CreateDraft.body>;
export type CreateDraftMetadataParam = FromSchema<typeof schemas.CreateDraft.metadata>;
export type CreateDraftReplyBodyParam = FromSchema<typeof schemas.CreateDraftReply.body>;
export type CreateDraftReplyMetadataParam = FromSchema<typeof schemas.CreateDraftReply.metadata>;
export type CreateDraftReplyResponse200 = FromSchema<typeof schemas.CreateDraftReply.response['200']>;
export type CreateDraftResponse200 = FromSchema<typeof schemas.CreateDraft.response['200']>;
export type CreateFolderBodyParam = FromSchema<typeof schemas.CreateFolder.body>;
export type CreateFolderResponse201 = FromSchema<typeof schemas.CreateFolder.response['201']>;
export type CreateGroupBodyParam = FromSchema<typeof schemas.CreateGroup.body>;
export type CreateInboxBodyParam = FromSchema<typeof schemas.CreateInbox.body>;
export type CreateLinkBodyParam = FromSchema<typeof schemas.CreateLink.body>;
export type CreateLinkResponse201 = FromSchema<typeof schemas.CreateLink.response['201']>;
export type CreateMessageBodyParam = FromSchema<typeof schemas.CreateMessage.body>;
export type CreateMessageMetadataParam = FromSchema<typeof schemas.CreateMessage.metadata>;
export type CreateMessageReplyBodyParam = FromSchema<typeof schemas.CreateMessageReply.body>;
export type CreateMessageReplyMetadataParam = FromSchema<typeof schemas.CreateMessageReply.metadata>;
export type CreateMessageReplyResponse202 = FromSchema<typeof schemas.CreateMessageReply.response['202']>;
export type CreateMessageResponse202 = FromSchema<typeof schemas.CreateMessage.response['202']>;
export type CreateMessageTemplateBodyParam = FromSchema<typeof schemas.CreateMessageTemplate.body>;
export type CreateMessageTemplateResponse201 = FromSchema<typeof schemas.CreateMessageTemplate.response['201']>;
export type CreateShiftBodyParam = FromSchema<typeof schemas.CreateShift.body>;
export type CreateShiftResponse201 = FromSchema<typeof schemas.CreateShift.response['201']>;
export type CreateTagBodyParam = FromSchema<typeof schemas.CreateTag.body>;
export type CreateTagResponse201 = FromSchema<typeof schemas.CreateTag.response['201']>;
export type CreateTeamContactBodyParam = FromSchema<typeof schemas.CreateTeamContact.body>;
export type CreateTeamContactMetadataParam = FromSchema<typeof schemas.CreateTeamContact.metadata>;
export type CreateTeamContactResponse201 = FromSchema<typeof schemas.CreateTeamContact.response['201']>;
export type CreateTeamFolderBodyParam = FromSchema<typeof schemas.CreateTeamFolder.body>;
export type CreateTeamFolderMetadataParam = FromSchema<typeof schemas.CreateTeamFolder.metadata>;
export type CreateTeamFolderResponse201 = FromSchema<typeof schemas.CreateTeamFolder.response['201']>;
export type CreateTeamGroupBodyParam = FromSchema<typeof schemas.CreateTeamGroup.body>;
export type CreateTeamGroupMetadataParam = FromSchema<typeof schemas.CreateTeamGroup.metadata>;
export type CreateTeamInboxBodyParam = FromSchema<typeof schemas.CreateTeamInbox.body>;
export type CreateTeamInboxMetadataParam = FromSchema<typeof schemas.CreateTeamInbox.metadata>;
export type CreateTeamMessageTemplateBodyParam = FromSchema<typeof schemas.CreateTeamMessageTemplate.body>;
export type CreateTeamMessageTemplateMetadataParam = FromSchema<typeof schemas.CreateTeamMessageTemplate.metadata>;
export type CreateTeamMessageTemplateResponse201 = FromSchema<typeof schemas.CreateTeamMessageTemplate.response['201']>;
export type CreateTeamShiftBodyParam = FromSchema<typeof schemas.CreateTeamShift.body>;
export type CreateTeamShiftMetadataParam = FromSchema<typeof schemas.CreateTeamShift.metadata>;
export type CreateTeamShiftResponse201 = FromSchema<typeof schemas.CreateTeamShift.response['201']>;
export type CreateTeamSignatureBodyParam = FromSchema<typeof schemas.CreateTeamSignature.body>;
export type CreateTeamSignatureMetadataParam = FromSchema<typeof schemas.CreateTeamSignature.metadata>;
export type CreateTeamSignatureResponse201 = FromSchema<typeof schemas.CreateTeamSignature.response['201']>;
export type CreateTeamTagBodyParam = FromSchema<typeof schemas.CreateTeamTag.body>;
export type CreateTeamTagMetadataParam = FromSchema<typeof schemas.CreateTeamTag.metadata>;
export type CreateTeamTagResponse201 = FromSchema<typeof schemas.CreateTeamTag.response['201']>;
export type CreateTeammateContactBodyParam = FromSchema<typeof schemas.CreateTeammateContact.body>;
export type CreateTeammateContactMetadataParam = FromSchema<typeof schemas.CreateTeammateContact.metadata>;
export type CreateTeammateContactResponse201 = FromSchema<typeof schemas.CreateTeammateContact.response['201']>;
export type CreateTeammateFolderBodyParam = FromSchema<typeof schemas.CreateTeammateFolder.body>;
export type CreateTeammateFolderMetadataParam = FromSchema<typeof schemas.CreateTeammateFolder.metadata>;
export type CreateTeammateFolderResponse201 = FromSchema<typeof schemas.CreateTeammateFolder.response['201']>;
export type CreateTeammateGroupBodyParam = FromSchema<typeof schemas.CreateTeammateGroup.body>;
export type CreateTeammateGroupMetadataParam = FromSchema<typeof schemas.CreateTeammateGroup.metadata>;
export type CreateTeammateMessageTemplateBodyParam = FromSchema<typeof schemas.CreateTeammateMessageTemplate.body>;
export type CreateTeammateMessageTemplateMetadataParam = FromSchema<typeof schemas.CreateTeammateMessageTemplate.metadata>;
export type CreateTeammateMessageTemplateResponse201 = FromSchema<typeof schemas.CreateTeammateMessageTemplate.response['201']>;
export type CreateTeammateSignatureBodyParam = FromSchema<typeof schemas.CreateTeammateSignature.body>;
export type CreateTeammateSignatureMetadataParam = FromSchema<typeof schemas.CreateTeammateSignature.metadata>;
export type CreateTeammateSignatureResponse201 = FromSchema<typeof schemas.CreateTeammateSignature.response['201']>;
export type CreateTeammateTagBodyParam = FromSchema<typeof schemas.CreateTeammateTag.body>;
export type CreateTeammateTagMetadataParam = FromSchema<typeof schemas.CreateTeammateTag.metadata>;
export type CreateTeammateTagResponse201 = FromSchema<typeof schemas.CreateTeammateTag.response['201']>;
export type DeleteAContactMetadataParam = FromSchema<typeof schemas.DeleteAContact.metadata>;
export type DeleteAnAccountMetadataParam = FromSchema<typeof schemas.DeleteAnAccount.metadata>;
export type DeleteContactHandleBodyParam = FromSchema<typeof schemas.DeleteContactHandle.body>;
export type DeleteContactHandleMetadataParam = FromSchema<typeof schemas.DeleteContactHandle.metadata>;
export type DeleteConversationFollowersBodyParam = FromSchema<typeof schemas.DeleteConversationFollowers.body>;
export type DeleteConversationFollowersMetadataParam = FromSchema<typeof schemas.DeleteConversationFollowers.metadata>;
export type DeleteDraftBodyParam = FromSchema<typeof schemas.DeleteDraft.body>;
export type DeleteDraftMetadataParam = FromSchema<typeof schemas.DeleteDraft.metadata>;
export type DeleteFolderMetadataParam = FromSchema<typeof schemas.DeleteFolder.metadata>;
export type DeleteFolderResponse202 = FromSchema<typeof schemas.DeleteFolder.response['202']>;
export type DeleteGroupMetadataParam = FromSchema<typeof schemas.DeleteGroup.metadata>;
export type DeleteMessageTemplateMetadataParam = FromSchema<typeof schemas.DeleteMessageTemplate.metadata>;
export type DeleteSignatureMetadataParam = FromSchema<typeof schemas.DeleteSignature.metadata>;
export type DeleteTagMetadataParam = FromSchema<typeof schemas.DeleteTag.metadata>;
export type DownloadAttachmentMetadataParam = FromSchema<typeof schemas.DownloadAttachment.metadata>;
export type DownloadAttachmentResponse200 = FromSchema<typeof schemas.DownloadAttachment.response['200']>;
export type EditDraftBodyParam = FromSchema<typeof schemas.EditDraft.body>;
export type EditDraftMetadataParam = FromSchema<typeof schemas.EditDraft.metadata>;
export type EditDraftResponse200 = FromSchema<typeof schemas.EditDraft.response['200']>;
export type FetchAnAccountMetadataParam = FromSchema<typeof schemas.FetchAnAccount.metadata>;
export type FetchAnAccountResponse200 = FromSchema<typeof schemas.FetchAnAccount.response['200']>;
export type GetAnalyticsExportMetadataParam = FromSchema<typeof schemas.GetAnalyticsExport.metadata>;
export type GetAnalyticsExportResponse200 = FromSchema<typeof schemas.GetAnalyticsExport.response['200']>;
export type GetAnalyticsReportMetadataParam = FromSchema<typeof schemas.GetAnalyticsReport.metadata>;
export type GetAnalyticsReportResponse200 = FromSchema<typeof schemas.GetAnalyticsReport.response['200']>;
export type GetChannelMetadataParam = FromSchema<typeof schemas.GetChannel.metadata>;
export type GetChannelResponse200 = FromSchema<typeof schemas.GetChannel.response['200']>;
export type GetChildFoldersMetadataParam = FromSchema<typeof schemas.GetChildFolders.metadata>;
export type GetChildFoldersResponse200 = FromSchema<typeof schemas.GetChildFolders.response['200']>;
export type GetChildTemplatesMetadataParam = FromSchema<typeof schemas.GetChildTemplates.metadata>;
export type GetChildTemplatesResponse200 = FromSchema<typeof schemas.GetChildTemplates.response['200']>;
export type GetCommentMetadataParam = FromSchema<typeof schemas.GetComment.metadata>;
export type GetCommentResponse200 = FromSchema<typeof schemas.GetComment.response['200']>;
export type GetContactMetadataParam = FromSchema<typeof schemas.GetContact.metadata>;
export type GetContactResponse200 = FromSchema<typeof schemas.GetContact.response['200']>;
export type GetConversationByIdMetadataParam = FromSchema<typeof schemas.GetConversationById.metadata>;
export type GetConversationByIdResponse200 = FromSchema<typeof schemas.GetConversationById.response['200']>;
export type GetEventMetadataParam = FromSchema<typeof schemas.GetEvent.metadata>;
export type GetEventResponse200 = FromSchema<typeof schemas.GetEvent.response['200']>;
export type GetFolderMetadataParam = FromSchema<typeof schemas.GetFolder.metadata>;
export type GetFolderResponse200 = FromSchema<typeof schemas.GetFolder.response['200']>;
export type GetInboxMetadataParam = FromSchema<typeof schemas.GetInbox.metadata>;
export type GetInboxResponse200 = FromSchema<typeof schemas.GetInbox.response['200']>;
export type GetLinkMetadataParam = FromSchema<typeof schemas.GetLink.metadata>;
export type GetLinkResponse200 = FromSchema<typeof schemas.GetLink.response['200']>;
export type GetMessageMetadataParam = FromSchema<typeof schemas.GetMessage.metadata>;
export type GetMessageResponse200 = FromSchema<typeof schemas.GetMessage.response['200']>;
export type GetMessageSeenStatusMetadataParam = FromSchema<typeof schemas.GetMessageSeenStatus.metadata>;
export type GetMessageSeenStatusResponse200 = FromSchema<typeof schemas.GetMessageSeenStatus.response['200']>;
export type GetMessageTemplateMetadataParam = FromSchema<typeof schemas.GetMessageTemplate.metadata>;
export type GetMessageTemplateResponse200 = FromSchema<typeof schemas.GetMessageTemplate.response['200']>;
export type GetRuleMetadataParam = FromSchema<typeof schemas.GetRule.metadata>;
export type GetRuleResponse200 = FromSchema<typeof schemas.GetRule.response['200']>;
export type GetShiftMetadataParam = FromSchema<typeof schemas.GetShift.metadata>;
export type GetShiftResponse200 = FromSchema<typeof schemas.GetShift.response['200']>;
export type GetSignaturesMetadataParam = FromSchema<typeof schemas.GetSignatures.metadata>;
export type GetSignaturesResponse200 = FromSchema<typeof schemas.GetSignatures.response['200']>;
export type GetTagMetadataParam = FromSchema<typeof schemas.GetTag.metadata>;
export type GetTagResponse200 = FromSchema<typeof schemas.GetTag.response['200']>;
export type GetTeamMetadataParam = FromSchema<typeof schemas.GetTeam.metadata>;
export type GetTeamResponse200 = FromSchema<typeof schemas.GetTeam.response['200']>;
export type GetTeammateMetadataParam = FromSchema<typeof schemas.GetTeammate.metadata>;
export type GetTeammateResponse200 = FromSchema<typeof schemas.GetTeammate.response['200']>;
export type ImportInboxMessageBodyParam = FromSchema<typeof schemas.ImportInboxMessage.body>;
export type ImportInboxMessageMetadataParam = FromSchema<typeof schemas.ImportInboxMessage.metadata>;
export type ImportInboxMessageResponse202 = FromSchema<typeof schemas.ImportInboxMessage.response['202']>;
export type ListAccountContactsMetadataParam = FromSchema<typeof schemas.ListAccountContacts.metadata>;
export type ListAccountContactsResponse200 = FromSchema<typeof schemas.ListAccountContacts.response['200']>;
export type ListAccountCustomFieldsResponse200 = FromSchema<typeof schemas.ListAccountCustomFields.response['200']>;
export type ListAccountsMetadataParam = FromSchema<typeof schemas.ListAccounts.metadata>;
export type ListAccountsResponse200 = FromSchema<typeof schemas.ListAccounts.response['200']>;
export type ListAllCompanyRulesResponse200 = FromSchema<typeof schemas.ListAllCompanyRules.response['200']>;
export type ListAssignedConversationsMetadataParam = FromSchema<typeof schemas.ListAssignedConversations.metadata>;
export type ListAssignedConversationsResponse200 = FromSchema<typeof schemas.ListAssignedConversations.response['200']>;
export type ListChannelsResponse200 = FromSchema<typeof schemas.ListChannels.response['200']>;
export type ListCommentMentionsMetadataParam = FromSchema<typeof schemas.ListCommentMentions.metadata>;
export type ListCommentMentionsResponse200 = FromSchema<typeof schemas.ListCommentMentions.response['200']>;
export type ListCompanyTagsMetadataParam = FromSchema<typeof schemas.ListCompanyTags.metadata>;
export type ListCompanyTagsResponse200 = FromSchema<typeof schemas.ListCompanyTags.response['200']>;
export type ListContactConversationsMetadataParam = FromSchema<typeof schemas.ListContactConversations.metadata>;
export type ListContactConversationsResponse200 = FromSchema<typeof schemas.ListContactConversations.response['200']>;
export type ListContactCustomFieldsResponse200 = FromSchema<typeof schemas.ListContactCustomFields.response['200']>;
export type ListContactsInGroupMetadataParam = FromSchema<typeof schemas.ListContactsInGroup.metadata>;
export type ListContactsInGroupResponse200 = FromSchema<typeof schemas.ListContactsInGroup.response['200']>;
export type ListContactsMetadataParam = FromSchema<typeof schemas.ListContacts.metadata>;
export type ListContactsResponse200 = FromSchema<typeof schemas.ListContacts.response['200']>;
export type ListConversationCommentsMetadataParam = FromSchema<typeof schemas.ListConversationComments.metadata>;
export type ListConversationCommentsResponse200 = FromSchema<typeof schemas.ListConversationComments.response['200']>;
export type ListConversationCustomFieldsResponse200 = FromSchema<typeof schemas.ListConversationCustomFields.response['200']>;
export type ListConversationDraftsMetadataParam = FromSchema<typeof schemas.ListConversationDrafts.metadata>;
export type ListConversationDraftsResponse200 = FromSchema<typeof schemas.ListConversationDrafts.response['200']>;
export type ListConversationEventsMetadataParam = FromSchema<typeof schemas.ListConversationEvents.metadata>;
export type ListConversationEventsResponse200 = FromSchema<typeof schemas.ListConversationEvents.response['200']>;
export type ListConversationFollowersMetadataParam = FromSchema<typeof schemas.ListConversationFollowers.metadata>;
export type ListConversationFollowersResponse200 = FromSchema<typeof schemas.ListConversationFollowers.response['200']>;
export type ListConversationInboxesMetadataParam = FromSchema<typeof schemas.ListConversationInboxes.metadata>;
export type ListConversationInboxesResponse200 = FromSchema<typeof schemas.ListConversationInboxes.response['200']>;
export type ListConversationMessagesMetadataParam = FromSchema<typeof schemas.ListConversationMessages.metadata>;
export type ListConversationMessagesResponse200 = FromSchema<typeof schemas.ListConversationMessages.response['200']>;
export type ListConversationsMetadataParam = FromSchema<typeof schemas.ListConversations.metadata>;
export type ListConversationsResponse200 = FromSchema<typeof schemas.ListConversations.response['200']>;
export type ListCustomFieldsResponse200 = FromSchema<typeof schemas.ListCustomFields.response['200']>;
export type ListEventsMetadataParam = FromSchema<typeof schemas.ListEvents.metadata>;
export type ListEventsResponse200 = FromSchema<typeof schemas.ListEvents.response['200']>;
export type ListFoldersMetadataParam = FromSchema<typeof schemas.ListFolders.metadata>;
export type ListFoldersResponse200 = FromSchema<typeof schemas.ListFolders.response['200']>;
export type ListGroupsResponse200 = FromSchema<typeof schemas.ListGroups.response['200']>;
export type ListInboxAccessMetadataParam = FromSchema<typeof schemas.ListInboxAccess.metadata>;
export type ListInboxAccessResponse200 = FromSchema<typeof schemas.ListInboxAccess.response['200']>;
export type ListInboxChannelsMetadataParam = FromSchema<typeof schemas.ListInboxChannels.metadata>;
export type ListInboxChannelsResponse200 = FromSchema<typeof schemas.ListInboxChannels.response['200']>;
export type ListInboxConversationsMetadataParam = FromSchema<typeof schemas.ListInboxConversations.metadata>;
export type ListInboxConversationsResponse200 = FromSchema<typeof schemas.ListInboxConversations.response['200']>;
export type ListInboxCustomFieldsResponse200 = FromSchema<typeof schemas.ListInboxCustomFields.response['200']>;
export type ListInboxesResponse200 = FromSchema<typeof schemas.ListInboxes.response['200']>;
export type ListLinkConversationsMetadataParam = FromSchema<typeof schemas.ListLinkConversations.metadata>;
export type ListLinkConversationsResponse200 = FromSchema<typeof schemas.ListLinkConversations.response['200']>;
export type ListLinkCustomFieldsResponse200 = FromSchema<typeof schemas.ListLinkCustomFields.response['200']>;
export type ListLinksMetadataParam = FromSchema<typeof schemas.ListLinks.metadata>;
export type ListLinksResponse200 = FromSchema<typeof schemas.ListLinks.response['200']>;
export type ListMessageTemplatesMetadataParam = FromSchema<typeof schemas.ListMessageTemplates.metadata>;
export type ListMessageTemplatesResponse200 = FromSchema<typeof schemas.ListMessageTemplates.response['200']>;
export type ListNotesMetadataParam = FromSchema<typeof schemas.ListNotes.metadata>;
export type ListNotesResponse202 = FromSchema<typeof schemas.ListNotes.response['202']>;
export type ListRulesResponse200 = FromSchema<typeof schemas.ListRules.response['200']>;
export type ListShiftsResponse200 = FromSchema<typeof schemas.ListShifts.response['200']>;
export type ListShiftsTeammatesMetadataParam = FromSchema<typeof schemas.ListShiftsTeammates.metadata>;
export type ListShiftsTeammatesResponse200 = FromSchema<typeof schemas.ListShiftsTeammates.response['200']>;
export type ListTagChildrenMetadataParam = FromSchema<typeof schemas.ListTagChildren.metadata>;
export type ListTagChildrenResponse200 = FromSchema<typeof schemas.ListTagChildren.response['200']>;
export type ListTaggedConversationsMetadataParam = FromSchema<typeof schemas.ListTaggedConversations.metadata>;
export type ListTaggedConversationsResponse200 = FromSchema<typeof schemas.ListTaggedConversations.response['200']>;
export type ListTagsMetadataParam = FromSchema<typeof schemas.ListTags.metadata>;
export type ListTagsResponse200 = FromSchema<typeof schemas.ListTags.response['200']>;
export type ListTeamChannelsMetadataParam = FromSchema<typeof schemas.ListTeamChannels.metadata>;
export type ListTeamChannelsResponse200 = FromSchema<typeof schemas.ListTeamChannels.response['200']>;
export type ListTeamContactsMetadataParam = FromSchema<typeof schemas.ListTeamContacts.metadata>;
export type ListTeamContactsResponse200 = FromSchema<typeof schemas.ListTeamContacts.response['200']>;
export type ListTeamFoldersMetadataParam = FromSchema<typeof schemas.ListTeamFolders.metadata>;
export type ListTeamFoldersResponse200 = FromSchema<typeof schemas.ListTeamFolders.response['200']>;
export type ListTeamGroupsMetadataParam = FromSchema<typeof schemas.ListTeamGroups.metadata>;
export type ListTeamGroupsResponse200 = FromSchema<typeof schemas.ListTeamGroups.response['200']>;
export type ListTeamInboxesMetadataParam = FromSchema<typeof schemas.ListTeamInboxes.metadata>;
export type ListTeamInboxesResponse200 = FromSchema<typeof schemas.ListTeamInboxes.response['200']>;
export type ListTeamMessageTemplatesMetadataParam = FromSchema<typeof schemas.ListTeamMessageTemplates.metadata>;
export type ListTeamMessageTemplatesResponse200 = FromSchema<typeof schemas.ListTeamMessageTemplates.response['200']>;
export type ListTeamRulesMetadataParam = FromSchema<typeof schemas.ListTeamRules.metadata>;
export type ListTeamRulesResponse200 = FromSchema<typeof schemas.ListTeamRules.response['200']>;
export type ListTeamShiftsMetadataParam = FromSchema<typeof schemas.ListTeamShifts.metadata>;
export type ListTeamShiftsResponse200 = FromSchema<typeof schemas.ListTeamShifts.response['200']>;
export type ListTeamSignaturesMetadataParam = FromSchema<typeof schemas.ListTeamSignatures.metadata>;
export type ListTeamSignaturesResponse200 = FromSchema<typeof schemas.ListTeamSignatures.response['200']>;
export type ListTeamTagsMetadataParam = FromSchema<typeof schemas.ListTeamTags.metadata>;
export type ListTeamTagsResponse200 = FromSchema<typeof schemas.ListTeamTags.response['200']>;
export type ListTeammateChannelsMetadataParam = FromSchema<typeof schemas.ListTeammateChannels.metadata>;
export type ListTeammateChannelsResponse200 = FromSchema<typeof schemas.ListTeammateChannels.response['200']>;
export type ListTeammateContactsMetadataParam = FromSchema<typeof schemas.ListTeammateContacts.metadata>;
export type ListTeammateContactsResponse200 = FromSchema<typeof schemas.ListTeammateContacts.response['200']>;
export type ListTeammateCustomFieldsResponse200 = FromSchema<typeof schemas.ListTeammateCustomFields.response['200']>;
export type ListTeammateFoldersMetadataParam = FromSchema<typeof schemas.ListTeammateFolders.metadata>;
export type ListTeammateFoldersResponse200 = FromSchema<typeof schemas.ListTeammateFolders.response['200']>;
export type ListTeammateGroupsMetadataParam = FromSchema<typeof schemas.ListTeammateGroups.metadata>;
export type ListTeammateGroupsResponse200 = FromSchema<typeof schemas.ListTeammateGroups.response['200']>;
export type ListTeammateInboxesMetadataParam = FromSchema<typeof schemas.ListTeammateInboxes.metadata>;
export type ListTeammateInboxesResponse200 = FromSchema<typeof schemas.ListTeammateInboxes.response['200']>;
export type ListTeammateMessageTemplatesMetadataParam = FromSchema<typeof schemas.ListTeammateMessageTemplates.metadata>;
export type ListTeammateMessageTemplatesResponse200 = FromSchema<typeof schemas.ListTeammateMessageTemplates.response['200']>;
export type ListTeammateRulesMetadataParam = FromSchema<typeof schemas.ListTeammateRules.metadata>;
export type ListTeammateRulesResponse200 = FromSchema<typeof schemas.ListTeammateRules.response['200']>;
export type ListTeammateShiftsMetadataParam = FromSchema<typeof schemas.ListTeammateShifts.metadata>;
export type ListTeammateShiftsResponse200 = FromSchema<typeof schemas.ListTeammateShifts.response['200']>;
export type ListTeammateSignaturesMetadataParam = FromSchema<typeof schemas.ListTeammateSignatures.metadata>;
export type ListTeammateSignaturesResponse200 = FromSchema<typeof schemas.ListTeammateSignatures.response['200']>;
export type ListTeammateTagsMetadataParam = FromSchema<typeof schemas.ListTeammateTags.metadata>;
export type ListTeammateTagsResponse200 = FromSchema<typeof schemas.ListTeammateTags.response['200']>;
export type ListTeammatesResponse200 = FromSchema<typeof schemas.ListTeammates.response['200']>;
export type ListTeamsResponse200 = FromSchema<typeof schemas.ListTeams.response['200']>;
export type MarkMessageSeenBodyParam = FromSchema<typeof schemas.MarkMessageSeen.body>;
export type MarkMessageSeenMetadataParam = FromSchema<typeof schemas.MarkMessageSeen.metadata>;
export type MergeContactsBodyParam = FromSchema<typeof schemas.MergeContacts.body>;
export type MergeContactsResponse200 = FromSchema<typeof schemas.MergeContacts.response['200']>;
export type ReceiveCustomMessagesBodyParam = FromSchema<typeof schemas.ReceiveCustomMessages.body>;
export type ReceiveCustomMessagesMetadataParam = FromSchema<typeof schemas.ReceiveCustomMessages.metadata>;
export type ReceiveCustomMessagesResponse202 = FromSchema<typeof schemas.ReceiveCustomMessages.response['202']>;
export type RemoveContactFromAccountBodyParam = FromSchema<typeof schemas.RemoveContactFromAccount.body>;
export type RemoveContactFromAccountMetadataParam = FromSchema<typeof schemas.RemoveContactFromAccount.metadata>;
export type RemoveContactsFromGroupBodyParam = FromSchema<typeof schemas.RemoveContactsFromGroup.body>;
export type RemoveContactsFromGroupMetadataParam = FromSchema<typeof schemas.RemoveContactsFromGroup.metadata>;
export type RemoveConversationLinksBodyParam = FromSchema<typeof schemas.RemoveConversationLinks.body>;
export type RemoveConversationLinksMetadataParam = FromSchema<typeof schemas.RemoveConversationLinks.metadata>;
export type RemoveConversationTagBodyParam = FromSchema<typeof schemas.RemoveConversationTag.body>;
export type RemoveConversationTagMetadataParam = FromSchema<typeof schemas.RemoveConversationTag.metadata>;
export type RemoveTeammatesFromShiftBodyParam = FromSchema<typeof schemas.RemoveTeammatesFromShift.body>;
export type RemoveTeammatesFromShiftMetadataParam = FromSchema<typeof schemas.RemoveTeammatesFromShift.metadata>;
export type RemoveTeammatesFromTeamBodyParam = FromSchema<typeof schemas.RemoveTeammatesFromTeam.body>;
export type RemoveTeammatesFromTeamMetadataParam = FromSchema<typeof schemas.RemoveTeammatesFromTeam.metadata>;
export type RemovesInboxAccessBodyParam = FromSchema<typeof schemas.RemovesInboxAccess.body>;
export type RemovesInboxAccessMetadataParam = FromSchema<typeof schemas.RemovesInboxAccess.metadata>;
export type SearchConversationsMetadataParam = FromSchema<typeof schemas.SearchConversations.metadata>;
export type SearchConversationsResponse200 = FromSchema<typeof schemas.SearchConversations.response['200']>;
export type UpdateAContactBodyParam = FromSchema<typeof schemas.UpdateAContact.body>;
export type UpdateAContactMetadataParam = FromSchema<typeof schemas.UpdateAContact.metadata>;
export type UpdateALinkBodyParam = FromSchema<typeof schemas.UpdateALink.body>;
export type UpdateALinkMetadataParam = FromSchema<typeof schemas.UpdateALink.metadata>;
export type UpdateATagBodyParam = FromSchema<typeof schemas.UpdateATag.body>;
export type UpdateATagMetadataParam = FromSchema<typeof schemas.UpdateATag.metadata>;
export type UpdateAccountBodyParam = FromSchema<typeof schemas.UpdateAccount.body>;
export type UpdateAccountMetadataParam = FromSchema<typeof schemas.UpdateAccount.metadata>;
export type UpdateAccountResponse200 = FromSchema<typeof schemas.UpdateAccount.response['200']>;
export type UpdateChannelBodyParam = FromSchema<typeof schemas.UpdateChannel.body>;
export type UpdateChannelMetadataParam = FromSchema<typeof schemas.UpdateChannel.metadata>;
export type UpdateConversationAssigneeBodyParam = FromSchema<typeof schemas.UpdateConversationAssignee.body>;
export type UpdateConversationAssigneeMetadataParam = FromSchema<typeof schemas.UpdateConversationAssignee.metadata>;
export type UpdateConversationBodyParam = FromSchema<typeof schemas.UpdateConversation.body>;
export type UpdateConversationMetadataParam = FromSchema<typeof schemas.UpdateConversation.metadata>;
export type UpdateConversationRemindersBodyParam = FromSchema<typeof schemas.UpdateConversationReminders.body>;
export type UpdateConversationRemindersMetadataParam = FromSchema<typeof schemas.UpdateConversationReminders.metadata>;
export type UpdateFolderBodyParam = FromSchema<typeof schemas.UpdateFolder.body>;
export type UpdateFolderMetadataParam = FromSchema<typeof schemas.UpdateFolder.metadata>;
export type UpdateFolderResponse200 = FromSchema<typeof schemas.UpdateFolder.response['200']>;
export type UpdateMessageTemplateBodyParam = FromSchema<typeof schemas.UpdateMessageTemplate.body>;
export type UpdateMessageTemplateMetadataParam = FromSchema<typeof schemas.UpdateMessageTemplate.metadata>;
export type UpdateMessageTemplateResponse200 = FromSchema<typeof schemas.UpdateMessageTemplate.response['200']>;
export type UpdateShiftBodyParam = FromSchema<typeof schemas.UpdateShift.body>;
export type UpdateShiftMetadataParam = FromSchema<typeof schemas.UpdateShift.metadata>;
export type UpdateSignatureBodyParam = FromSchema<typeof schemas.UpdateSignature.body>;
export type UpdateSignatureMetadataParam = FromSchema<typeof schemas.UpdateSignature.metadata>;
export type UpdateSignatureResponse200 = FromSchema<typeof schemas.UpdateSignature.response['200']>;
export type UpdateTeammateBodyParam = FromSchema<typeof schemas.UpdateTeammate.body>;
export type UpdateTeammateMetadataParam = FromSchema<typeof schemas.UpdateTeammate.metadata>;
export type ValidateChannelMetadataParam = FromSchema<typeof schemas.ValidateChannel.metadata>;
export type ValidateChannelResponse202 = FromSchema<typeof schemas.ValidateChannel.response['202']>;
